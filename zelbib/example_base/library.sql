--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.1

-- Started on 2017-05-26 17:59:09

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3449 (class 1262 OID 27799)
-- Name: library; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE library WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'ru_RU.UTF-8' LC_CTYPE = 'ru_RU.UTF-8';


ALTER DATABASE library OWNER TO postgres;

\connect library

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 9 (class 2615 OID 27801)
-- Name: core; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA core;


ALTER SCHEMA core OWNER TO postgres;

--
-- TOC entry 8 (class 2615 OID 27800)
-- Name: library; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA library;


ALTER SCHEMA library OWNER TO postgres;

--
-- TOC entry 1 (class 3079 OID 13310)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3452 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = core, pg_catalog;

--
-- TOC entry 230 (class 1255 OID 27939)
-- Name: f_get_id(); Type: FUNCTION; Schema: core; Owner: postgres
--

CREATE FUNCTION f_get_id() RETURNS bigint
LANGUAGE sql SECURITY DEFINER
AS $$
select nextval('core.main_seq')::bigint;
$$;


ALTER FUNCTION core.f_get_id() OWNER TO postgres;

--
-- TOC entry 306 (class 1255 OID 28288)
-- Name: f_priv8check(character varying, character varying, character varying); Type: FUNCTION; Schema: core; Owner: postgres
--

CREATE FUNCTION f_priv8check(pv_login character varying, pv_pass character varying, pv_action character varying) RETURNS boolean
LANGUAGE plpgsql SECURITY DEFINER
AS $$
declare
  v_user_code     varchar;
  b_active        boolean;
  v_action_exist  varchar;
BEGIN

  --проверяем есть ли такой пользователь
  select u.code
  into v_user_code
  from core.t_user u
  where u.login = pv_login
        and   u.pass = pv_pass;

  --проверяем есть ли активное дейтсвие у роли у юзера
  select ra.action
  into v_action_exist
  from core.t_role_action ra
    join core.t_role r on ra.role = r.code
    join core.t_user_role ur on ur.role = r.code
    join core.t_user u on u.code = ur.user
  where ra.action = pv_action and u.code = v_user_code;

  if v_action_exist is not null then
    return true;
  else
    return false;
  end if;
END;
$$;


ALTER FUNCTION core.f_priv8check(pv_login character varying, pv_pass character varying, pv_action character varying) OWNER TO postgres;

SET search_path = library, pg_catalog;

--
-- TOC entry 231 (class 1255 OID 28094)
-- Name: f_about_city8add(bigint, character varying, timestamp without time zone); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_about_city8add(pn_id bigint, pv_caption character varying, pd_when timestamp without time zone) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_about_city
    (
      id,
      caption,
      "when"
    )
  VALUES (
    pn_id,
    pv_caption,
    pd_when
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_about_city8add(pn_id bigint, pv_caption character varying, pd_when timestamp without time zone) OWNER TO postgres;

--
-- TOC entry 233 (class 1255 OID 28096)
-- Name: f_about_city8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_about_city8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_about_city
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_about_city8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 234 (class 1255 OID 28097)
-- Name: f_about_city8mod(bigint, character varying, timestamp without time zone); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_about_city8mod(pn_id bigint, pv_caption character varying, pd_when timestamp without time zone) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_about_city8add(
        pn_id := core.f_get_id(),
        pv_caption := pv_caption,
        pd_when := pd_when
    );
  else
    n_id := library.f_about_city8upd(
        pn_id := pn_id,
        pv_caption := pv_caption,
        pd_when := pd_when
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_about_city8mod(pn_id bigint, pv_caption character varying, pd_when timestamp without time zone) OWNER TO postgres;

--
-- TOC entry 232 (class 1255 OID 28095)
-- Name: f_about_city8upd(bigint, character varying, timestamp without time zone); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_about_city8upd(pn_id bigint, pv_caption character varying, pd_when timestamp without time zone) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_about_city
  SET
    caption = pv_caption,
    "when" = pd_when
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_about_city8upd(pn_id bigint, pv_caption character varying, pd_when timestamp without time zone) OWNER TO postgres;

--
-- TOC entry 280 (class 1255 OID 28222)
-- Name: f_adress8add(bigint, character varying, character varying, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_adress8add(pn_id bigint, pv_adress character varying, pv_phone character varying, pv_email character varying, pv_site character varying, pv_telegram character varying, pv_watsapp character varying, pv_vk character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_adress
    (
      id,
      adress,
      phone,
      email,
      site,
      telegram,
      watsapp,
      vk
    )
  VALUES (
    pn_id,
    pv_adress,
    pv_phone,
    pv_email,
    pv_site,
    pv_telegram,
    pv_watsapp,
    pv_vk
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_adress8add(pn_id bigint, pv_adress character varying, pv_phone character varying, pv_email character varying, pv_site character varying, pv_telegram character varying, pv_watsapp character varying, pv_vk character varying) OWNER TO postgres;

--
-- TOC entry 282 (class 1255 OID 28225)
-- Name: f_adress8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_adress8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_adress
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_adress8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 287 (class 1255 OID 28226)
-- Name: f_adress8mod(bigint, character varying, character varying, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_adress8mod(pn_id bigint, pv_adress character varying, pv_phone character varying, pv_email character varying, pv_site character varying, pv_telegram character varying, pv_watsapp character varying, pv_vk character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_adress8add(
        pn_id := core.f_get_id(),
        pv_adress := pv_adress,
        pv_phone := pv_phone,
        pv_email := pv_email,
        pv_site := pv_site,
        pv_telegram := pv_telegram,
        pv_watsapp := pv_watsapp,
        pv_vk := pv_vk
    );
  else
    n_id := library.f_adress8upd(
        pn_id := pn_id,
        pv_adress := pv_adress,
        pv_phone := pv_phone,
        pv_email := pv_email,
        pv_site := pv_site,
        pv_telegram := pv_telegram,
        pv_watsapp := pv_watsapp,
        pv_vk := pv_vk
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_adress8mod(pn_id bigint, pv_adress character varying, pv_phone character varying, pv_email character varying, pv_site character varying, pv_telegram character varying, pv_watsapp character varying, pv_vk character varying) OWNER TO postgres;

--
-- TOC entry 281 (class 1255 OID 28223)
-- Name: f_adress8upd(bigint, character varying, character varying, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_adress8upd(pn_id bigint, pv_adress character varying, pv_phone character varying, pv_email character varying, pv_site character varying, pv_telegram character varying, pv_watsapp character varying, pv_vk character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_adress
  SET
    adress = pv_adress,
    phone = pv_phone,
    email = pv_email,
    site = pv_site,
    telegram = pv_telegram,
    watsapp = pv_watsapp,
    vk = pv_vk
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_adress8upd(pn_id bigint, pv_adress character varying, pv_phone character varying, pv_email character varying, pv_site character varying, pv_telegram character varying, pv_watsapp character varying, pv_vk character varying) OWNER TO postgres;

--
-- TOC entry 247 (class 1255 OID 28098)
-- Name: f_affiche8add(bigint, timestamp without time zone, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_affiche8add(pn_id bigint, pd_date_of timestamp without time zone, pv_content character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_affiche
    (
      id,
      date_of,
      content
    )
  VALUES (
    pn_id,
    pd_date_of,
    pv_content
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_affiche8add(pn_id bigint, pd_date_of timestamp without time zone, pv_content character varying) OWNER TO postgres;

--
-- TOC entry 249 (class 1255 OID 28100)
-- Name: f_affiche8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_affiche8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_affiche
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_affiche8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 250 (class 1255 OID 28101)
-- Name: f_affiche8mod(bigint, timestamp without time zone, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_affiche8mod(pn_id bigint, pd_date_of timestamp without time zone, pv_content character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null THEN
    n_id := library.f_affiche8add(
        pn_id := core.f_get_id(),
        pd_date_of := pd_date_of,
        pv_content := pv_content
    );
  else
    n_id := library.f_affiche8upd(
        pn_id := pn_id,
        pd_date_of := pd_date_of,
        pv_content := pv_content
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_affiche8mod(pn_id bigint, pd_date_of timestamp without time zone, pv_content character varying) OWNER TO postgres;

--
-- TOC entry 248 (class 1255 OID 28099)
-- Name: f_affiche8upd(bigint, timestamp without time zone, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_affiche8upd(pn_id bigint, pd_date_of timestamp without time zone, pv_content character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_affiche
  SET
    date_of = pd_date_of,
    content = pv_content
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_affiche8upd(pn_id bigint, pd_date_of timestamp without time zone, pv_content character varying) OWNER TO postgres;

--
-- TOC entry 309 (class 1255 OID 28324)
-- Name: f_album8add(bigint, date, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_album8add(pn_id bigint, pd_date_of date, pv_title character varying, pv_cover character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_album
    (
      id,
      date_of,
      title,
      cover
    )
  VALUES (
    pn_id,
    pd_date_of,
    pv_title,
    pv_cover
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_album8add(pn_id bigint, pd_date_of date, pv_title character varying, pv_cover character varying) OWNER TO postgres;

--
-- TOC entry 251 (class 1255 OID 28104)
-- Name: f_album8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_album8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_album
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_album8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 307 (class 1255 OID 28322)
-- Name: f_album8mod(bigint, date, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_album8mod(pn_id bigint, pd_date_of date, pv_title character varying, pv_cover character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_album8add(
        pn_id := core.f_get_id(),
        pd_date_of := pd_date_of,
        pv_title := pv_title,
        pv_cover := pv_cover
    );
  else
    n_id := library.f_album8upd(
        pn_id := pn_id,
        pd_date_of := pd_date_of,
        pv_title := pv_title,
        pv_cover :=pv_cover
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_album8mod(pn_id bigint, pd_date_of date, pv_title character varying, pv_cover character varying) OWNER TO postgres;

--
-- TOC entry 308 (class 1255 OID 28323)
-- Name: f_album8upd(bigint, date, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_album8upd(pn_id bigint, pd_date_of date, pv_title character varying, pv_cover character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_album
  SET
    date_of = pd_date_of,
    title = pv_title,
    cover = pv_cover
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_album8upd(pn_id bigint, pd_date_of date, pv_title character varying, pv_cover character varying) OWNER TO postgres;

--
-- TOC entry 252 (class 1255 OID 28106)
-- Name: f_article8add(bigint, timestamp without time zone, character varying, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_article8add(pn_id bigint, pd_date_of timestamp without time zone, pv_title character varying, pv_preview_annotation character varying, pv_content character varying, pv_preview_image character varying, pv_content_image character varying, pv_author character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_article
    (
      id,
      date_of,
      title,
      preview_annotation,
      content,
      preview_image,
      content_image,
      author
    )
  VALUES (
    pn_id,
    pd_date_of,
    pv_title,
    pv_preview_annotation,
    pv_content,
    pv_preview_image,
    pv_content_image,
    pv_author
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_article8add(pn_id bigint, pd_date_of timestamp without time zone, pv_title character varying, pv_preview_annotation character varying, pv_content character varying, pv_preview_image character varying, pv_content_image character varying, pv_author character varying) OWNER TO postgres;

--
-- TOC entry 254 (class 1255 OID 28108)
-- Name: f_article8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_article8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_article
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_article8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 255 (class 1255 OID 28109)
-- Name: f_article8mod(bigint, timestamp without time zone, character varying, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_article8mod(pn_id bigint, pd_date_of timestamp without time zone, pv_title character varying, pv_preview_annotation character varying, pv_content character varying, pv_preview_image character varying, pv_content_image character varying, pv_author character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_article8add(
        pn_id := core.f_get_id(),
        pd_date_of := pd_date_of,
        pv_title := pv_title,
        pv_preview_annotation := pv_preview_annotation,
        pv_content := pv_content,
        pv_preview_image := pv_preview_image,
        pv_content_image := pv_content_image,
        pv_author := pv_author
    );
  else
    n_id := library.f_article8upd(
        pn_id := pn_id,
        pd_date_of := pd_date_of,
        pv_title := pv_title,
        pv_preview_annotation := pv_preview_annotation,
        pv_content := pv_content,
        pv_preview_image := pv_preview_image,
        pv_content_image := pv_content_image,
        pv_author := pv_author
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_article8mod(pn_id bigint, pd_date_of timestamp without time zone, pv_title character varying, pv_preview_annotation character varying, pv_content character varying, pv_preview_image character varying, pv_content_image character varying, pv_author character varying) OWNER TO postgres;

--
-- TOC entry 253 (class 1255 OID 28107)
-- Name: f_article8upd(bigint, timestamp without time zone, character varying, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_article8upd(pn_id bigint, pd_date_of timestamp without time zone, pv_title character varying, pv_preview_annotation character varying, pv_content character varying, pv_preview_image character varying, pv_content_image character varying, pv_author character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_article
  SET
    date_of = pd_date_of,
    title = pv_title,
    preview_annotation = pv_preview_annotation,
    content = pv_content,
    preview_image = pv_preview_image,
    content_image = pv_content_image,
    author = pv_author
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_article8upd(pn_id bigint, pd_date_of timestamp without time zone, pv_title character varying, pv_preview_annotation character varying, pv_content character varying, pv_preview_image character varying, pv_content_image character varying, pv_author character varying) OWNER TO postgres;

--
-- TOC entry 283 (class 1255 OID 28227)
-- Name: f_club8add(bigint, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_club8add(pn_id bigint, pv_caption character varying, pv_description character varying, pv_dates character varying, pv_age character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_club
    (
      id,
      caption,
      description,
      dates,
      age
    )
  VALUES (
    pn_id,
    pv_caption,
    pv_description,
    pv_dates,
    pv_age
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_club8add(pn_id bigint, pv_caption character varying, pv_description character varying, pv_dates character varying, pv_age character varying) OWNER TO postgres;

--
-- TOC entry 285 (class 1255 OID 28229)
-- Name: f_club8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_club8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_club
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_club8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 304 (class 1255 OID 28230)
-- Name: f_club8mod(bigint, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_club8mod(pn_id bigint, pv_caption character varying, pv_description character varying, pv_dates character varying, pv_age character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_club8add(
        pn_id := core.f_get_id(),
        pv_caption := pv_caption,
        pv_description := pv_description,
        pv_dates := pv_dates,
        pv_age := pv_age
    );
  else
    n_id := library.f_club8upd(
        pn_id := pn_id,
        pv_caption := pv_caption,
        pv_description := pv_description,
        pv_dates := pv_dates,
        pv_age := pv_age
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_club8mod(pn_id bigint, pv_caption character varying, pv_description character varying, pv_dates character varying, pv_age character varying) OWNER TO postgres;

--
-- TOC entry 284 (class 1255 OID 28228)
-- Name: f_club8upd(bigint, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_club8upd(pn_id bigint, pv_caption character varying, pv_description character varying, pv_dates character varying, pv_age character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_club
  SET
    caption = pv_caption,
    description = pv_description,
    dates = pv_dates,
    age = pv_age
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_club8upd(pn_id bigint, pv_caption character varying, pv_description character varying, pv_dates character varying, pv_age character varying) OWNER TO postgres;

--
-- TOC entry 288 (class 1255 OID 28231)
-- Name: f_competition8add(bigint, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_competition8add(pn_id bigint, pv_caption character varying, pv_description character varying, pv_dates character varying, pv_age character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_competition
    (
      id,
      caption,
      description,
      dates,
      age
    )
  VALUES (
    pn_id,
    pv_caption,
    pv_description,
    pv_dates,
    pv_age
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_competition8add(pn_id bigint, pv_caption character varying, pv_description character varying, pv_dates character varying, pv_age character varying) OWNER TO postgres;

--
-- TOC entry 290 (class 1255 OID 28233)
-- Name: f_competition8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_competition8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_competition
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_competition8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 291 (class 1255 OID 28234)
-- Name: f_competition8mod(bigint, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_competition8mod(pn_id bigint, pv_caption character varying, pv_description character varying, pv_dates character varying, pv_age character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_competition8add(
        pn_id := core.f_get_id(),
        pv_caption := pv_caption,
        pv_description := pv_description,
        pv_dates := pv_dates,
        pv_age := pv_age
    );
  else
    n_id := library.f_competition8upd(
        pn_id := pn_id,
        pv_caption := pv_caption,
        pv_description := pv_description,
        pv_dates := pv_dates,
        pv_age := pv_age
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_competition8mod(pn_id bigint, pv_caption character varying, pv_description character varying, pv_dates character varying, pv_age character varying) OWNER TO postgres;

--
-- TOC entry 289 (class 1255 OID 28232)
-- Name: f_competition8upd(bigint, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_competition8upd(pn_id bigint, pv_caption character varying, pv_description character varying, pv_dates character varying, pv_age character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_competition
  SET
    caption = pv_caption,
    description = pv_description,
    dates = pv_dates,
    age = pv_age
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_competition8upd(pn_id bigint, pv_caption character varying, pv_description character varying, pv_dates character varying, pv_age character varying) OWNER TO postgres;

--
-- TOC entry 294 (class 1255 OID 28239)
-- Name: f_document8add(bigint, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_document8add(pn_id bigint, pv_caption character varying, pv_link character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_document
    (
      id,
      caption,
      link
    )
  VALUES (
    pn_id,
    pv_caption,
    pv_link
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_document8add(pn_id bigint, pv_caption character varying, pv_link character varying) OWNER TO postgres;

--
-- TOC entry 296 (class 1255 OID 28241)
-- Name: f_document8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_document8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_document
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_document8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 297 (class 1255 OID 28242)
-- Name: f_document8mod(bigint, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_document8mod(pn_id bigint, pv_caption character varying, pv_link character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_document8add(
        pn_id := core.f_get_id(),
        pv_caption := pv_caption,
        pv_link := pv_link
    );
  else
    n_id := library.f_document8upd(
        pn_id := pn_id,
        pv_caption := pv_caption,
        pv_link := pv_link
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_document8mod(pn_id bigint, pv_caption character varying, pv_link character varying) OWNER TO postgres;

--
-- TOC entry 295 (class 1255 OID 28240)
-- Name: f_document8upd(bigint, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_document8upd(pn_id bigint, pv_caption character varying, pv_link character varying) RETURNS record
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_document
  SET
    caption = pv_caption,
    link = pv_link
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_document8upd(pn_id bigint, pv_caption character varying, pv_link character varying) OWNER TO postgres;

--
-- TOC entry 276 (class 1255 OID 28148)
-- Name: f_flashbook8add(bigint, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_flashbook8add(pn_id bigint, pv_caption character varying, pv_link character varying, pv_img character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_flashbook
    (
      id,
      caption,
      link,
      img
    )
  VALUES (
    pn_id,
    pv_caption,
    pv_link,
    pv_img
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_flashbook8add(pn_id bigint, pv_caption character varying, pv_link character varying, pv_img character varying) OWNER TO postgres;

--
-- TOC entry 256 (class 1255 OID 28112)
-- Name: f_flashbook8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_flashbook8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_flashbook
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_flashbook8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 274 (class 1255 OID 28144)
-- Name: f_flashbook8mod(bigint, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_flashbook8mod(pn_id bigint, pv_caption character varying, pv_link character varying, pv_img character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_flashbook8add(
        pn_id := core.f_get_id(),
        pv_caption := pv_caption,
        pv_link := pv_link,
        pv_img := pv_img
    );
  else
    n_id := library.f_flashbook8upd(
        pn_id := pn_id,
        pv_caption := pv_caption,
        pv_link := pv_link,
        pv_img := pv_img
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_flashbook8mod(pn_id bigint, pv_caption character varying, pv_link character varying, pv_img character varying) OWNER TO postgres;

--
-- TOC entry 275 (class 1255 OID 28147)
-- Name: f_flashbook8upd(bigint, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_flashbook8upd(pn_id bigint, pv_caption character varying, pv_link character varying, pv_img character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_flashbook
  SET
    caption = pv_caption,
    link = pv_link,
    img = pv_img
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_flashbook8upd(pn_id bigint, pv_caption character varying, pv_link character varying, pv_img character varying) OWNER TO postgres;

--
-- TOC entry 314 (class 1255 OID 28352)
-- Name: f_guest_book8add(bigint, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_guest_book8add(pn_id bigint, pv_author character varying, pv_content character varying, pv_email character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_guest_book
    (
      id,
      author,
      content,
      email,
      date_of
    )
  VALUES (
    pn_id,
    pv_author,
    pv_content,
    pv_email,
    CURRENT_TIMESTAMP
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_guest_book8add(pn_id bigint, pv_author character varying, pv_content character varying, pv_email character varying) OWNER TO postgres;

--
-- TOC entry 257 (class 1255 OID 28116)
-- Name: f_guest_book8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_guest_book8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_guest_book
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_guest_book8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 315 (class 1255 OID 28354)
-- Name: f_guest_book8mod(bigint, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_guest_book8mod(pn_id bigint, pv_author character varying, pv_content character varying, pv_email character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_guest_book8add(
        pn_id := core.f_get_id(),
        pv_author := pv_author,
        pv_content := pv_content,
        pv_email := pv_email
    );
  else
    n_id := library.f_guest_book8upd(
        pn_id := pn_id,
        pv_author := pv_author,
        pv_content := pv_content,
        pv_email := pv_email
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_guest_book8mod(pn_id bigint, pv_author character varying, pv_content character varying, pv_email character varying) OWNER TO postgres;

--
-- TOC entry 313 (class 1255 OID 28353)
-- Name: f_guest_book8upd(bigint, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_guest_book8upd(pn_id bigint, pv_author character varying, pv_content character varying, pv_email character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_guest_book
  SET
    author = pv_author,
    content = pv_content,
    email = pv_email,
    date_of = CURRENT_TIMESTAMP
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_guest_book8upd(pn_id bigint, pv_author character varying, pv_content character varying, pv_email character varying) OWNER TO postgres;

--
-- TOC entry 258 (class 1255 OID 28118)
-- Name: f_material8add(bigint, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_material8add(pn_id bigint, pv_caption character varying, pv_quantity character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_material
    (
      id,
      caption,
      quantity
    )
  VALUES (
    pn_id,
    pv_caption,
    pv_quantity
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_material8add(pn_id bigint, pv_caption character varying, pv_quantity character varying) OWNER TO postgres;

--
-- TOC entry 260 (class 1255 OID 28120)
-- Name: f_material8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_material8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_material
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_material8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 298 (class 1255 OID 28243)
-- Name: f_material8mod(bigint, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_material8mod(pn_id bigint, pv_caption character varying, pv_quantity character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null THEN
    n_id := library.f_material8add(
        pn_id := core.f_get_id(),
        pv_caption := pv_caption,
        pv_quantity := pv_quantity
    );
  else
    n_id := library.f_material8upd(
        pn_id := pn_id,
        pv_caption := pv_caption,
        pv_quantity := pv_quantity
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_material8mod(pn_id bigint, pv_caption character varying, pv_quantity character varying) OWNER TO postgres;

--
-- TOC entry 259 (class 1255 OID 28119)
-- Name: f_material8upd(bigint, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_material8upd(pn_id bigint, pv_caption character varying, pv_quantity character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_material
  SET
    caption = pv_caption,
    quantity = pv_quantity
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_material8upd(pn_id bigint, pv_caption character varying, pv_quantity character varying) OWNER TO postgres;

--
-- TOC entry 277 (class 1255 OID 28218)
-- Name: f_mode8add(bigint, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_mode8add(pn_id bigint, pv_holiday character varying, pv_working character varying, pv_lunch character varying, pv_last_day character varying, pv_season character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_mode
    (
      id,
      holiday,
      working,
      lunch,
      last_day,
      season
    )
  VALUES (
    pn_id,
    pv_holiday,
    pv_working,
    pv_lunch,
    pv_last_day,
    pv_season
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_mode8add(pn_id bigint, pv_holiday character varying, pv_working character varying, pv_lunch character varying, pv_last_day character varying, pv_season character varying) OWNER TO postgres;

--
-- TOC entry 279 (class 1255 OID 28220)
-- Name: f_mode8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_mode8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_mode
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_mode8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 286 (class 1255 OID 28221)
-- Name: f_mode8mod(bigint, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_mode8mod(pn_id bigint, pv_holiday character varying, pv_working character varying, pv_lunch character varying, pv_last_day character varying, pv_season character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_mode8add(
        pn_id := core.f_get_id(),
        pv_holiday := pv_holiday,
        pv_working := pv_working,
        pv_lunch := pv_lunch,
        pv_last_day := pv_last_day,
        pv_season := pv_season
    );
  else
    n_id = library.f_mode8upd(
        pn_id := pn_id,
        pv_holiday := pv_holiday,
        pv_working := pv_working,
        pv_lunch := pv_lunch,
        pv_last_day := pv_last_day,
        pv_season := pv_season
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_mode8mod(pn_id bigint, pv_holiday character varying, pv_working character varying, pv_lunch character varying, pv_last_day character varying, pv_season character varying) OWNER TO postgres;

--
-- TOC entry 278 (class 1255 OID 28219)
-- Name: f_mode8upd(bigint, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_mode8upd(pn_id bigint, pv_holiday character varying, pv_working character varying, pv_lunch character varying, pv_last_day character varying, pv_season character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_mode
  SET
    holiday = pv_holiday,
    working = pv_working,
    lunch = pv_lunch,
    last_day = pv_last_day,
    season = pv_season
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_mode8upd(pn_id bigint, pv_holiday character varying, pv_working character varying, pv_lunch character varying, pv_last_day character varying, pv_season character varying) OWNER TO postgres;

--
-- TOC entry 261 (class 1255 OID 28122)
-- Name: f_periodic8add(bigint, character varying, character varying, bigint, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_periodic8add(pn_id bigint, pv_caption character varying, pv_period character varying, pn_periodic_type bigint, pv_language character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_periodic
    (
      id,
      caption,
      period,
      periodic_type,
      language
    )
  VALUES (
    pn_id,
    pv_caption,
    pv_period,
    pn_periodic_type,
    pv_language
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_periodic8add(pn_id bigint, pv_caption character varying, pv_period character varying, pn_periodic_type bigint, pv_language character varying) OWNER TO postgres;

--
-- TOC entry 263 (class 1255 OID 28124)
-- Name: f_periodic8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_periodic8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_periodic
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_periodic8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 264 (class 1255 OID 28125)
-- Name: f_periodic8mod(bigint, character varying, character varying, bigint, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_periodic8mod(pn_id bigint, pv_caption character varying, pv_period character varying, pn_periodic_type bigint, pv_language character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_periodic8add(
        pn_id := core.f_get_id(),
        pv_caption := pv_caption,
        pv_period := pv_period,
        pn_periodic_type := pn_periodic_type,
        pv_language := pv_language
    );
  else
    n_id := library.f_periodic8upd(
        pn_id := pn_id,
        pv_caption := pv_caption,
        pv_period := pv_period,
        pn_periodic_type := pn_periodic_type,
        pv_language := pv_language
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_periodic8mod(pn_id bigint, pv_caption character varying, pv_period character varying, pn_periodic_type bigint, pv_language character varying) OWNER TO postgres;

--
-- TOC entry 262 (class 1255 OID 28123)
-- Name: f_periodic8upd(bigint, character varying, character varying, bigint, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_periodic8upd(pn_id bigint, pv_caption character varying, pv_period character varying, pn_periodic_type bigint, pv_language character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_periodic
  SET
    caption = pv_caption,
    period = pv_period,
    periodic_type = pn_periodic_type,
    language = pv_language
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_periodic8upd(pn_id bigint, pv_caption character varying, pv_period character varying, pn_periodic_type bigint, pv_language character varying) OWNER TO postgres;

--
-- TOC entry 265 (class 1255 OID 28126)
-- Name: f_periodic_type8add(bigint, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_periodic_type8add(pn_id bigint, pv_caption character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_periodic_type
    (
      id,
      caption
    )
  VALUES (
    pn_id,
    pv_caption
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_periodic_type8add(pn_id bigint, pv_caption character varying) OWNER TO postgres;

--
-- TOC entry 267 (class 1255 OID 28128)
-- Name: f_periodic_type8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_periodic_type8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_periodic_type
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_periodic_type8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 268 (class 1255 OID 28129)
-- Name: f_periodic_type8mod(bigint, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_periodic_type8mod(pn_id bigint, pv_caption character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_periodic_type8add(
        pn_id := core.f_get_id(),
        pv_caption := pv_caption
    );
  else
    n_id := library.f_periodic_type8upd(
        pn_id := pn_id,
        pv_caption := pv_caption
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_periodic_type8mod(pn_id bigint, pv_caption character varying) OWNER TO postgres;

--
-- TOC entry 266 (class 1255 OID 28127)
-- Name: f_periodic_type8upd(bigint, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_periodic_type8upd(pn_id bigint, pv_caption character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_periodic_type
  SET
    caption = pv_caption
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_periodic_type8upd(pn_id bigint, pv_caption character varying) OWNER TO postgres;

--
-- TOC entry 310 (class 1255 OID 28329)
-- Name: f_photo8add(bigint, character varying, character varying, bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_photo8add(pn_id bigint, pv_caption character varying, pv_link character varying, pn_pid bigint) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_photo
    (
      id,
      caption,
      link,
      pid
    )
  VALUES (
    pn_id,
    pv_caption,
    pv_link,
    pn_pid
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_photo8add(pn_id bigint, pv_caption character varying, pv_link character varying, pn_pid bigint) OWNER TO postgres;

--
-- TOC entry 312 (class 1255 OID 28332)
-- Name: f_photo8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_photo8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.v_photo
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_photo8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 305 (class 1255 OID 28333)
-- Name: f_photo8mod(bigint, character varying, character varying, bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_photo8mod(pn_id bigint, pv_caption character varying, pv_link character varying, pn_pid bigint) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_photo8add(
        pn_id := core.f_get_id(),
        pv_caption := pv_caption,
        pv_link := pv_link,
        pn_pid := pn_pid
    );
  else
    n_id := library.f_photo8upd(
        pn_id := pn_id,
        pv_caption := pv_caption,
        pv_link := pv_link,
        pn_pid := pn_pid
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_photo8mod(pn_id bigint, pv_caption character varying, pv_link character varying, pn_pid bigint) OWNER TO postgres;

--
-- TOC entry 311 (class 1255 OID 28331)
-- Name: f_photo8upd(bigint, character varying, character varying, bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_photo8upd(pn_id bigint, pv_caption character varying, pv_link character varying, pn_pid bigint) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.v_photo
  SET
    id = pn_id,
    caption = pv_caption,
    link = pv_link,
    pid = pn_pid
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_photo8upd(pn_id bigint, pv_caption character varying, pv_link character varying, pn_pid bigint) OWNER TO postgres;

--
-- TOC entry 301 (class 1255 OID 28259)
-- Name: f_pricelist8add(bigint, character varying, bigint, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_pricelist8add(pn_id bigint, pv_caption character varying, pn_pid bigint, pv_cost character varying, pv_per_one character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_pricelist
    (
      id,
      caption,
      pid,
      cost,
      per_one
    )
  VALUES (
    pn_id,
    pv_caption,
    pn_pid,
    pv_cost,
    pv_per_one
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_pricelist8add(pn_id bigint, pv_caption character varying, pn_pid bigint, pv_cost character varying, pv_per_one character varying) OWNER TO postgres;

--
-- TOC entry 269 (class 1255 OID 28132)
-- Name: f_pricelist8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_pricelist8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_pricelist
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_pricelist8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 299 (class 1255 OID 28257)
-- Name: f_pricelist8mod(bigint, character varying, bigint, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_pricelist8mod(pn_id bigint, pv_caption character varying, pn_pid bigint, pv_cost character varying, pv_per_one character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_pricelist8add(
        pn_id := core.f_get_id(),
        pv_caption := pv_caption,
        pn_pid := pn_pid,
        pv_cost := pv_cost,
        pv_per_one := pv_per_one
    );
  else
    n_id := library.f_pricelist8upd(
        pn_id := pn_id,
        pv_caption := pv_caption,
        pn_pid := pn_pid,
        pv_cost := pv_cost,
        pv_per_one := pv_per_one
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_pricelist8mod(pn_id bigint, pv_caption character varying, pn_pid bigint, pv_cost character varying, pv_per_one character varying) OWNER TO postgres;

--
-- TOC entry 300 (class 1255 OID 28258)
-- Name: f_pricelist8upd(bigint, character varying, bigint, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_pricelist8upd(pn_id bigint, pv_caption character varying, pn_pid bigint, pv_cost character varying, pv_per_one character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_pricelist
  SET
    caption = pv_caption,
    pid = pn_pid,
    cost = pv_cost,
    per_one = pv_per_one
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_pricelist8upd(pn_id bigint, pv_caption character varying, pn_pid bigint, pv_cost character varying, pv_per_one character varying) OWNER TO postgres;

--
-- TOC entry 303 (class 1255 OID 28268)
-- Name: f_recommendation8add(bigint, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_recommendation8add(pn_id bigint, pv_rubric character varying, pv_caption character varying, pv_description character varying, pv_link character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_recommendation
    (
      id,
      rubric,
      caption,
      description,
      link
    )
  VALUES (
    pn_id,
    pv_rubric,
    pv_caption,
    pv_description,
    pv_link
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_recommendation8add(pn_id bigint, pv_rubric character varying, pv_caption character varying, pv_description character varying, pv_link character varying) OWNER TO postgres;

--
-- TOC entry 293 (class 1255 OID 28237)
-- Name: f_recommendation8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_recommendation8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_recommendation
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_recommendation8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 302 (class 1255 OID 28238)
-- Name: f_recommendation8mod(bigint, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_recommendation8mod(pn_id bigint, pv_rubric character varying, pv_caption character varying, pv_description character varying, pv_link character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_recommendation8add(
        pn_id := core.f_get_id(),
        pv_rubric := pv_rubric,
        pv_caption := pv_caption,
        pv_description := pv_description,
        pv_link := pv_link
    );
  else
    n_id := library.f_recommendation8upd(
        pn_id := pn_id,
        pv_rubric := pv_rubric,
        pv_caption := pv_caption,
        pv_description := pv_description,
        pv_link := pv_link
    );
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION library.f_recommendation8mod(pn_id bigint, pv_rubric character varying, pv_caption character varying, pv_description character varying, pv_link character varying) OWNER TO postgres;

--
-- TOC entry 292 (class 1255 OID 28236)
-- Name: f_recommendation8upd(bigint, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_recommendation8upd(pn_id bigint, pv_rubric character varying, pv_caption character varying, pv_description character varying, pv_link character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_recommendation
  SET
    rubric = pv_rubric,
    caption = pv_caption,
    description = pv_description,
    link = pv_link
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_recommendation8upd(pn_id bigint, pv_rubric character varying, pv_caption character varying, pv_description character varying, pv_link character varying) OWNER TO postgres;

--
-- TOC entry 270 (class 1255 OID 28134)
-- Name: f_today8add(bigint, date, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_today8add(pn_id bigint, pd_date_of date, pv_title character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  INSERT INTO
    library.t_today
    (
      id,
      date_of,
      title
    )
  VALUES (
    pn_id,
    pd_date_of,
    pv_title
  );
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_today8add(pn_id bigint, pd_date_of date, pv_title character varying) OWNER TO postgres;

--
-- TOC entry 272 (class 1255 OID 28136)
-- Name: f_today8del(bigint); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_today8del(pn_id bigint) RETURNS void
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  DELETE FROM
    library.t_today
  WHERE
    id = pn_id
  ;
END;
$$;


ALTER FUNCTION library.f_today8del(pn_id bigint) OWNER TO postgres;

--
-- TOC entry 273 (class 1255 OID 28137)
-- Name: f_today8mod(bigint, date, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_today8mod(pn_id bigint, pd_date_of date, pv_title character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := library.f_today8add(
        pn_id := core.f_get_id(),
        pd_date_of := pd_date_of,
        pv_title := pv_title
    );
  else
    n_id := library.f_today8upd(
        pn_id := pn_id,
        pd_date_of := pd_date_of,
        pv_title := pv_title
    );
  end if;
  return  n_id;
END;
$$;


ALTER FUNCTION library.f_today8mod(pn_id bigint, pd_date_of date, pv_title character varying) OWNER TO postgres;

--
-- TOC entry 271 (class 1255 OID 28135)
-- Name: f_today8upd(bigint, date, character varying); Type: FUNCTION; Schema: library; Owner: postgres
--

CREATE FUNCTION f_today8upd(pn_id bigint, pd_date_of date, pv_title character varying) RETURNS bigint
LANGUAGE plpgsql SECURITY DEFINER
AS $$
BEGIN
  UPDATE
    library.t_today
  SET
    date_of = pd_date_of,
    title = pv_title
  WHERE
    id = pn_id
  ;
  return pn_id;
END;
$$;


ALTER FUNCTION library.f_today8upd(pn_id bigint, pd_date_of date, pv_title character varying) OWNER TO postgres;

SET search_path = core, pg_catalog;

--
-- TOC entry 201 (class 1259 OID 27936)
-- Name: main_seq; Type: SEQUENCE; Schema: core; Owner: postgres
--

CREATE SEQUENCE main_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE main_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 27905)
-- Name: t_action; Type: TABLE; Schema: core; Owner: postgres
--

CREATE TABLE t_action (
  id bigint NOT NULL,
  caption character varying(30) NOT NULL,
  code character varying(30) NOT NULL,
  "table" character varying(30) NOT NULL,
  action character varying(30) NOT NULL
);
ALTER TABLE ONLY t_action ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_action ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_action ALTER COLUMN code SET STATISTICS 0;
ALTER TABLE ONLY t_action ALTER COLUMN "table" SET STATISTICS 0;
ALTER TABLE ONLY t_action ALTER COLUMN action SET STATISTICS 0;


ALTER TABLE t_action OWNER TO postgres;

--
-- TOC entry 3453 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN t_action.id; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_action.id IS 'id';


--
-- TOC entry 3454 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN t_action.caption; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_action.caption IS 'Название действия';


--
-- TOC entry 3455 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN t_action.code; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_action.code IS 'Код действия';


--
-- TOC entry 3456 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN t_action."table"; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_action."table" IS 'Название таблицы';


--
-- TOC entry 3457 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN t_action.action; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_action.action IS 'Действие';


--
-- TOC entry 200 (class 1259 OID 27928)
-- Name: t_log; Type: TABLE; Schema: core; Owner: postgres
--

CREATE TABLE t_log (
  id bigint NOT NULL,
  "user" character varying,
  role character varying NOT NULL,
  action character varying NOT NULL,
  "table" character varying(30) NOT NULL,
  object character varying(20) NOT NULL,
  it_was character varying NOT NULL,
  it_now character varying NOT NULL,
  sql_revert character varying NOT NULL,
  timed timestamp(6) without time zone NOT NULL
);
ALTER TABLE ONLY t_log ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN "user" SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN role SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN action SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN "table" SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN object SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN it_was SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN it_now SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN sql_revert SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN timed SET STATISTICS 0;


ALTER TABLE t_log OWNER TO postgres;

--
-- TOC entry 3458 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN t_log.id; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_log.id IS 'id';


--
-- TOC entry 3459 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN t_log."user"; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_log."user" IS 'Пользователь';


--
-- TOC entry 3460 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN t_log.role; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_log.role IS 'Роль';


--
-- TOC entry 3461 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN t_log.action; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_log.action IS 'Действие';


--
-- TOC entry 3462 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN t_log."table"; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_log."table" IS 'Изменяемая таблица';


--
-- TOC entry 3463 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN t_log.object; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_log.object IS 'Изменяемый обьект';


--
-- TOC entry 3464 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN t_log.it_was; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_log.it_was IS 'Что было';


--
-- TOC entry 3465 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN t_log.it_now; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_log.it_now IS 'Что стало';


--
-- TOC entry 3466 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN t_log.sql_revert; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_log.sql_revert IS 'Скрипт для возврата';


--
-- TOC entry 3467 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN t_log.timed; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_log.timed IS 'Дата изменения';


--
-- TOC entry 196 (class 1259 OID 27898)
-- Name: t_role; Type: TABLE; Schema: core; Owner: postgres
--

CREATE TABLE t_role (
  id bigint NOT NULL,
  caption character varying(30) NOT NULL,
  code character varying(30) NOT NULL,
  active boolean NOT NULL
);
ALTER TABLE ONLY t_role ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_role ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_role ALTER COLUMN code SET STATISTICS 0;
ALTER TABLE ONLY t_role ALTER COLUMN active SET STATISTICS 0;


ALTER TABLE t_role OWNER TO postgres;

--
-- TOC entry 3468 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN t_role.id; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_role.id IS 'id';


--
-- TOC entry 3469 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN t_role.caption; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_role.caption IS 'Название роли';


--
-- TOC entry 3470 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN t_role.code; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_role.code IS 'Код роли';


--
-- TOC entry 3471 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN t_role.active; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_role.active IS 'Активность';


--
-- TOC entry 198 (class 1259 OID 27912)
-- Name: t_role_action; Type: TABLE; Schema: core; Owner: postgres
--

CREATE TABLE t_role_action (
  id bigint NOT NULL,
  role character varying NOT NULL,
  action character varying NOT NULL
);
ALTER TABLE ONLY t_role_action ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_role_action ALTER COLUMN role SET STATISTICS 0;
ALTER TABLE ONLY t_role_action ALTER COLUMN action SET STATISTICS 0;


ALTER TABLE t_role_action OWNER TO postgres;

--
-- TOC entry 3472 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN t_role_action.id; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_role_action.id IS 'id';


--
-- TOC entry 3473 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN t_role_action.role; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_role_action.role IS 'Роль';


--
-- TOC entry 3474 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN t_role_action.action; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_role_action.action IS 'Действие';


--
-- TOC entry 195 (class 1259 OID 27883)
-- Name: t_user; Type: TABLE; Schema: core; Owner: postgres
--

CREATE TABLE t_user (
  id bigint NOT NULL,
  caption character varying(30) NOT NULL,
  code character varying(30) NOT NULL,
  login character varying(30) NOT NULL,
  pass character varying NOT NULL,
  active boolean NOT NULL
);
ALTER TABLE ONLY t_user ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_user ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_user ALTER COLUMN code SET STATISTICS 0;
ALTER TABLE ONLY t_user ALTER COLUMN login SET STATISTICS 0;
ALTER TABLE ONLY t_user ALTER COLUMN pass SET STATISTICS 0;
ALTER TABLE ONLY t_user ALTER COLUMN active SET STATISTICS 0;


ALTER TABLE t_user OWNER TO postgres;

--
-- TOC entry 3475 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN t_user.id; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_user.id IS 'id';


--
-- TOC entry 3476 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN t_user.caption; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_user.caption IS 'Имя пользователя';


--
-- TOC entry 3477 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN t_user.code; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_user.code IS 'Код пользователя';


--
-- TOC entry 3478 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN t_user.login; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_user.login IS 'Логин пользователя';


--
-- TOC entry 3479 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN t_user.pass; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_user.pass IS 'Пароль пользователя';


--
-- TOC entry 3480 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN t_user.active; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_user.active IS 'Активность';


--
-- TOC entry 199 (class 1259 OID 27920)
-- Name: t_user_role; Type: TABLE; Schema: core; Owner: postgres
--

CREATE TABLE t_user_role (
  id bigint NOT NULL,
  "user" character varying NOT NULL,
  role character varying NOT NULL,
  active boolean NOT NULL
);
ALTER TABLE ONLY t_user_role ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_user_role ALTER COLUMN "user" SET STATISTICS 0;
ALTER TABLE ONLY t_user_role ALTER COLUMN role SET STATISTICS 0;
ALTER TABLE ONLY t_user_role ALTER COLUMN active SET STATISTICS 0;


ALTER TABLE t_user_role OWNER TO postgres;

--
-- TOC entry 3481 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN t_user_role.id; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_user_role.id IS 'id';


--
-- TOC entry 3482 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN t_user_role."user"; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_user_role."user" IS 'Пользователь';


--
-- TOC entry 3483 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN t_user_role.role; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_user_role.role IS 'Роль';


--
-- TOC entry 3484 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN t_user_role.active; Type: COMMENT; Schema: core; Owner: postgres
--

COMMENT ON COLUMN t_user_role.active IS 'Активность';


--
-- TOC entry 224 (class 1259 OID 28269)
-- Name: v_user; Type: VIEW; Schema: core; Owner: postgres
--

CREATE VIEW v_user AS
  SELECT t_user.id,
    t_user.caption,
    t_user.code,
    t_user.login,
    t_user.pass,
    t_user.active
  FROM t_user;


ALTER TABLE v_user OWNER TO postgres;

SET search_path = library, pg_catalog;

--
-- TOC entry 194 (class 1259 OID 27875)
-- Name: t_about_city; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_about_city (
  id bigint NOT NULL,
  caption character varying NOT NULL,
  "when" timestamp(6) without time zone NOT NULL
);
ALTER TABLE ONLY t_about_city ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_about_city ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_about_city ALTER COLUMN "when" SET STATISTICS 0;


ALTER TABLE t_about_city OWNER TO postgres;

--
-- TOC entry 3485 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN t_about_city.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_about_city.id IS 'id';


--
-- TOC entry 3486 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN t_about_city.caption; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_about_city.caption IS 'Событие';


--
-- TOC entry 3487 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN t_about_city."when"; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_about_city."when" IS 'Дата события';


--
-- TOC entry 210 (class 1259 OID 28154)
-- Name: t_adress; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_adress (
  id bigint NOT NULL,
  adress character varying(100),
  phone character varying(100),
  email character varying(100),
  site character varying(100),
  telegram character varying(100),
  watsapp character varying(100),
  vk character varying(100)
);
ALTER TABLE ONLY t_adress ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_adress ALTER COLUMN adress SET STATISTICS 0;
ALTER TABLE ONLY t_adress ALTER COLUMN phone SET STATISTICS 0;
ALTER TABLE ONLY t_adress ALTER COLUMN email SET STATISTICS 0;
ALTER TABLE ONLY t_adress ALTER COLUMN site SET STATISTICS 0;
ALTER TABLE ONLY t_adress ALTER COLUMN telegram SET STATISTICS 0;
ALTER TABLE ONLY t_adress ALTER COLUMN watsapp SET STATISTICS 0;
ALTER TABLE ONLY t_adress ALTER COLUMN vk SET STATISTICS 0;


ALTER TABLE t_adress OWNER TO postgres;

--
-- TOC entry 3488 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN t_adress.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_adress.id IS 'id';


--
-- TOC entry 3489 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN t_adress.adress; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_adress.adress IS 'Адрес';


--
-- TOC entry 3490 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN t_adress.phone; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_adress.phone IS 'Телефон';


--
-- TOC entry 3491 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN t_adress.email; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_adress.email IS 'Почта';


--
-- TOC entry 3492 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN t_adress.site; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_adress.site IS 'Сайт';


--
-- TOC entry 3493 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN t_adress.telegram; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_adress.telegram IS 'Телеграм';


--
-- TOC entry 3494 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN t_adress.watsapp; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_adress.watsapp IS 'Ватсап';


--
-- TOC entry 3495 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN t_adress.vk; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_adress.vk IS 'Вконтакте';


--
-- TOC entry 187 (class 1259 OID 27828)
-- Name: t_affiche; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_affiche (
  id bigint NOT NULL,
  date_of timestamp(6) without time zone NOT NULL,
  content character varying(100) NOT NULL
);
ALTER TABLE ONLY t_affiche ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_affiche ALTER COLUMN date_of SET STATISTICS 0;
ALTER TABLE ONLY t_affiche ALTER COLUMN content SET STATISTICS 0;


ALTER TABLE t_affiche OWNER TO postgres;

--
-- TOC entry 3496 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_affiche.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_affiche.id IS 'id';


--
-- TOC entry 3497 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_affiche.date_of; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_affiche.date_of IS 'Дата мероприятия';


--
-- TOC entry 3498 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_affiche.content; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_affiche.content IS 'Описание';


--
-- TOC entry 183 (class 1259 OID 27802)
-- Name: t_album; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_album (
  id bigint NOT NULL,
  date_of date NOT NULL,
  title character varying(30) NOT NULL,
  cover character varying
);
ALTER TABLE ONLY t_album ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_album ALTER COLUMN date_of SET STATISTICS 0;
ALTER TABLE ONLY t_album ALTER COLUMN title SET STATISTICS 0;


ALTER TABLE t_album OWNER TO postgres;

--
-- TOC entry 3499 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_album.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_album.id IS 'id';


--
-- TOC entry 3500 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_album.date_of; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_album.date_of IS 'Дата публикации';


--
-- TOC entry 3501 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_album.title; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_album.title IS 'title';


--
-- TOC entry 3502 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_album.cover; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_album.cover IS 'Обложка';


--
-- TOC entry 184 (class 1259 OID 27807)
-- Name: t_article; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_article (
  id bigint NOT NULL,
  date_of timestamp without time zone,
  title character varying NOT NULL,
  preview_annotation character varying NOT NULL,
  content character varying NOT NULL,
  preview_image character varying NOT NULL,
  content_image character varying NOT NULL,
  author character varying NOT NULL
);
ALTER TABLE ONLY t_article ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_article ALTER COLUMN date_of SET STATISTICS 0;
ALTER TABLE ONLY t_article ALTER COLUMN title SET STATISTICS 0;
ALTER TABLE ONLY t_article ALTER COLUMN preview_annotation SET STATISTICS 0;
ALTER TABLE ONLY t_article ALTER COLUMN content SET STATISTICS 0;
ALTER TABLE ONLY t_article ALTER COLUMN preview_image SET STATISTICS 0;
ALTER TABLE ONLY t_article ALTER COLUMN content_image SET STATISTICS 0;
ALTER TABLE ONLY t_article ALTER COLUMN author SET STATISTICS 0;


ALTER TABLE t_article OWNER TO postgres;

--
-- TOC entry 3503 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN t_article.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_article.id IS 'id';


--
-- TOC entry 3504 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN t_article.date_of; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_article.date_of IS 'Дата публикации';


--
-- TOC entry 3505 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN t_article.title; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_article.title IS 'title';


--
-- TOC entry 3506 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN t_article.preview_annotation; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_article.preview_annotation IS 'Аннотация';


--
-- TOC entry 3507 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN t_article.content; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_article.content IS 'Статья';


--
-- TOC entry 3508 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN t_article.preview_image; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_article.preview_image IS 'Картинка для превью';


--
-- TOC entry 3509 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN t_article.content_image; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_article.content_image IS 'Картинка для статьи';


--
-- TOC entry 3510 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN t_article.author; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_article.author IS 'Автор статьи';


--
-- TOC entry 211 (class 1259 OID 28162)
-- Name: t_club; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_club (
  id bigint NOT NULL,
  caption character varying(100),
  description character varying(200),
  dates character varying(100),
  age character varying(100)
);
ALTER TABLE ONLY t_club ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_club ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_club ALTER COLUMN description SET STATISTICS 0;
ALTER TABLE ONLY t_club ALTER COLUMN dates SET STATISTICS 0;
ALTER TABLE ONLY t_club ALTER COLUMN age SET STATISTICS 0;


ALTER TABLE t_club OWNER TO postgres;

--
-- TOC entry 3511 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN t_club.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_club.id IS 'id';


--
-- TOC entry 3512 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN t_club.caption; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_club.caption IS 'Название';


--
-- TOC entry 3513 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN t_club.description; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_club.description IS 'Описание';


--
-- TOC entry 3514 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN t_club.dates; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_club.dates IS 'Даты проведения';


--
-- TOC entry 3515 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN t_club.age; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_club.age IS 'Категория';


--
-- TOC entry 212 (class 1259 OID 28170)
-- Name: t_competition; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_competition (
  id bigint NOT NULL,
  caption character varying(100),
  description character varying(500),
  dates character varying(100),
  age character varying(100)
);
ALTER TABLE ONLY t_competition ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_competition ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_competition ALTER COLUMN description SET STATISTICS 0;
ALTER TABLE ONLY t_competition ALTER COLUMN dates SET STATISTICS 0;
ALTER TABLE ONLY t_competition ALTER COLUMN age SET STATISTICS 0;


ALTER TABLE t_competition OWNER TO postgres;

--
-- TOC entry 3516 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN t_competition.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_competition.id IS 'id';


--
-- TOC entry 3517 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN t_competition.caption; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_competition.caption IS 'Название';


--
-- TOC entry 3518 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN t_competition.description; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_competition.description IS 'Описание';


--
-- TOC entry 3519 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN t_competition.dates; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_competition.dates IS 'Даты проведения';


--
-- TOC entry 3520 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN t_competition.age; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_competition.age IS 'Категория';


--
-- TOC entry 214 (class 1259 OID 28186)
-- Name: t_document; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_document (
  id bigint NOT NULL,
  caption character varying(100),
  link character varying(500)
);
ALTER TABLE ONLY t_document ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_document ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_document ALTER COLUMN link SET STATISTICS 0;


ALTER TABLE t_document OWNER TO postgres;

--
-- TOC entry 3521 (class 0 OID 0)
-- Dependencies: 214
-- Name: COLUMN t_document.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_document.id IS 'id';


--
-- TOC entry 3522 (class 0 OID 0)
-- Dependencies: 214
-- Name: COLUMN t_document.caption; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_document.caption IS 'Документ';


--
-- TOC entry 3523 (class 0 OID 0)
-- Dependencies: 214
-- Name: COLUMN t_document.link; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_document.link IS 'Ссылка';


--
-- TOC entry 189 (class 1259 OID 27841)
-- Name: t_flashbook; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_flashbook (
  id bigint NOT NULL,
  caption character varying(20) NOT NULL,
  link character varying NOT NULL,
  img character varying NOT NULL
);
ALTER TABLE ONLY t_flashbook ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_flashbook ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_flashbook ALTER COLUMN link SET STATISTICS 0;


ALTER TABLE t_flashbook OWNER TO postgres;

--
-- TOC entry 3524 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN t_flashbook.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_flashbook.id IS 'id';


--
-- TOC entry 3525 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN t_flashbook.caption; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_flashbook.caption IS 'Название';


--
-- TOC entry 3526 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN t_flashbook.link; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_flashbook.link IS 'Ссылка';


--
-- TOC entry 3527 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN t_flashbook.img; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_flashbook.img IS 'Картинка';


--
-- TOC entry 186 (class 1259 OID 27820)
-- Name: t_guest_book; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_guest_book (
  id bigint NOT NULL,
  author character varying(100) NOT NULL,
  content character varying(500) NOT NULL,
  email character varying(50),
  date_of timestamp(0) without time zone
);
ALTER TABLE ONLY t_guest_book ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_guest_book ALTER COLUMN author SET STATISTICS 0;
ALTER TABLE ONLY t_guest_book ALTER COLUMN content SET STATISTICS 0;
ALTER TABLE ONLY t_guest_book ALTER COLUMN email SET STATISTICS 0;


ALTER TABLE t_guest_book OWNER TO postgres;

--
-- TOC entry 3528 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN t_guest_book.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_guest_book.id IS 'id';


--
-- TOC entry 3529 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN t_guest_book.author; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_guest_book.author IS 'Автор';


--
-- TOC entry 3530 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN t_guest_book.content; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_guest_book.content IS 'Запись';


--
-- TOC entry 3531 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN t_guest_book.email; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_guest_book.email IS 'Почта';


--
-- TOC entry 3532 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN t_guest_book.date_of; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_guest_book.date_of IS 'Дата записи';


--
-- TOC entry 188 (class 1259 OID 27833)
-- Name: t_log; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_log (
  id bigint NOT NULL,
  "user" character varying NOT NULL,
  role character varying NOT NULL,
  action character varying NOT NULL,
  "table" character varying(20) NOT NULL,
  object character varying(20) NOT NULL,
  it_was character varying NOT NULL,
  it_now character varying NOT NULL,
  sql_revert character varying NOT NULL,
  timed timestamp(6) without time zone NOT NULL
);
ALTER TABLE ONLY t_log ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN "user" SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN role SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN action SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN "table" SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN object SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN it_was SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN it_now SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN sql_revert SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN timed SET STATISTICS 0;


ALTER TABLE t_log OWNER TO postgres;

--
-- TOC entry 3533 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_log.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_log.id IS 'id';


--
-- TOC entry 3534 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_log."user"; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_log."user" IS 'Пользователь';


--
-- TOC entry 3535 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_log.role; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_log.role IS 'Роль';


--
-- TOC entry 3536 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_log.action; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_log.action IS 'Действие';


--
-- TOC entry 3537 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_log."table"; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_log."table" IS 'Изменяемая таблица';


--
-- TOC entry 3538 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_log.object; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_log.object IS 'Изменяемый обьект';


--
-- TOC entry 3539 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_log.it_was; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_log.it_was IS 'Что было';


--
-- TOC entry 3540 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_log.it_now; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_log.it_now IS 'Что стало';


--
-- TOC entry 3541 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_log.sql_revert; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_log.sql_revert IS 'Скрипт для возврата';


--
-- TOC entry 3542 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_log.timed; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_log.timed IS 'Дата изменения';


--
-- TOC entry 193 (class 1259 OID 27867)
-- Name: t_material; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_material (
  id bigint NOT NULL,
  caption character varying NOT NULL,
  quantity character varying(20) NOT NULL
);
ALTER TABLE ONLY t_material ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_material ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_material ALTER COLUMN quantity SET STATISTICS 0;


ALTER TABLE t_material OWNER TO postgres;

--
-- TOC entry 3543 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN t_material.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_material.id IS 'id';


--
-- TOC entry 3544 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN t_material.caption; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_material.caption IS 'Название имущества';


--
-- TOC entry 3545 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN t_material.quantity; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_material.quantity IS 'Количество';


--
-- TOC entry 209 (class 1259 OID 28149)
-- Name: t_mode; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_mode (
  id bigint NOT NULL,
  holiday character varying(100),
  working character varying(100),
  lunch character varying(100),
  last_day character varying(100),
  season character varying(20) NOT NULL
);
ALTER TABLE ONLY t_mode ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_mode ALTER COLUMN holiday SET STATISTICS 0;
ALTER TABLE ONLY t_mode ALTER COLUMN working SET STATISTICS 0;
ALTER TABLE ONLY t_mode ALTER COLUMN lunch SET STATISTICS 0;
ALTER TABLE ONLY t_mode ALTER COLUMN last_day SET STATISTICS 0;
ALTER TABLE ONLY t_mode ALTER COLUMN season SET STATISTICS 0;


ALTER TABLE t_mode OWNER TO postgres;

--
-- TOC entry 3546 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN t_mode.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_mode.id IS 'id';


--
-- TOC entry 3547 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN t_mode.holiday; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_mode.holiday IS 'Выходные дни';


--
-- TOC entry 3548 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN t_mode.working; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_mode.working IS 'Рабочие часы';


--
-- TOC entry 3549 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN t_mode.lunch; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_mode.lunch IS 'Обед';


--
-- TOC entry 3550 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN t_mode.last_day; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_mode.last_day IS 'Последний день месяца';


--
-- TOC entry 3551 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN t_mode.season; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_mode.season IS 'Сезон';


--
-- TOC entry 191 (class 1259 OID 27857)
-- Name: t_periodic; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_periodic (
  id bigint NOT NULL,
  caption character varying(100) NOT NULL,
  period character varying(20) NOT NULL,
  periodic_type bigint NOT NULL,
  language character varying(20) NOT NULL
);
ALTER TABLE ONLY t_periodic ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_periodic ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_periodic ALTER COLUMN period SET STATISTICS 0;
ALTER TABLE ONLY t_periodic ALTER COLUMN periodic_type SET STATISTICS 0;
ALTER TABLE ONLY t_periodic ALTER COLUMN language SET STATISTICS 0;


ALTER TABLE t_periodic OWNER TO postgres;

--
-- TOC entry 3552 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN t_periodic.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_periodic.id IS 'id';


--
-- TOC entry 3553 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN t_periodic.caption; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_periodic.caption IS 'Название издания';


--
-- TOC entry 3554 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN t_periodic.period; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_periodic.period IS 'Периодичность выхода';


--
-- TOC entry 3555 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN t_periodic.periodic_type; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_periodic.periodic_type IS 'Тип издания';


--
-- TOC entry 3556 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN t_periodic.language; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_periodic.language IS 'Язык издания';


--
-- TOC entry 192 (class 1259 OID 27862)
-- Name: t_periodic_type; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_periodic_type (
  id bigint NOT NULL,
  caption character varying(20) NOT NULL
);
ALTER TABLE ONLY t_periodic_type ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_periodic_type ALTER COLUMN caption SET STATISTICS 0;


ALTER TABLE t_periodic_type OWNER TO postgres;

--
-- TOC entry 3557 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN t_periodic_type.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_periodic_type.id IS 'id';


--
-- TOC entry 3558 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN t_periodic_type.caption; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_periodic_type.caption IS 'Название типа издания';


--
-- TOC entry 226 (class 1259 OID 28297)
-- Name: t_photo; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_photo (
  id bigint NOT NULL,
  caption character varying(100),
  link character varying,
  pid bigint
);
ALTER TABLE ONLY t_photo ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_photo ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_photo ALTER COLUMN link SET STATISTICS 0;
ALTER TABLE ONLY t_photo ALTER COLUMN pid SET STATISTICS 0;


ALTER TABLE t_photo OWNER TO postgres;

--
-- TOC entry 3559 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN t_photo.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_photo.id IS 'id';


--
-- TOC entry 3560 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN t_photo.caption; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_photo.caption IS 'Название';


--
-- TOC entry 3561 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN t_photo.link; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_photo.link IS 'Ссылка';


--
-- TOC entry 3562 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN t_photo.pid; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_photo.pid IS 'Альбом';


--
-- TOC entry 190 (class 1259 OID 27849)
-- Name: t_pricelist; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_pricelist (
  id bigint NOT NULL,
  caption character varying NOT NULL,
  pid bigint,
  cost character varying(20) NOT NULL,
  per_one character varying(20)
);
ALTER TABLE ONLY t_pricelist ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_pricelist ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_pricelist ALTER COLUMN pid SET STATISTICS 0;
ALTER TABLE ONLY t_pricelist ALTER COLUMN cost SET STATISTICS 0;


ALTER TABLE t_pricelist OWNER TO postgres;

--
-- TOC entry 3563 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN t_pricelist.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_pricelist.id IS 'id';


--
-- TOC entry 3564 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN t_pricelist.caption; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_pricelist.caption IS 'Название услуги';


--
-- TOC entry 3565 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN t_pricelist.pid; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_pricelist.pid IS 'Подразделение услуг';


--
-- TOC entry 3566 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN t_pricelist.cost; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_pricelist.cost IS 'Стоимость';


--
-- TOC entry 3567 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN t_pricelist.per_one; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_pricelist.per_one IS 'За единицу';


--
-- TOC entry 213 (class 1259 OID 28178)
-- Name: t_recommendation; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_recommendation (
  id bigint NOT NULL,
  rubric character varying(100),
  caption character varying(100),
  description character varying(500),
  link character varying(500)
);
ALTER TABLE ONLY t_recommendation ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_recommendation ALTER COLUMN rubric SET STATISTICS 0;
ALTER TABLE ONLY t_recommendation ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_recommendation ALTER COLUMN description SET STATISTICS 0;
ALTER TABLE ONLY t_recommendation ALTER COLUMN link SET STATISTICS 0;


ALTER TABLE t_recommendation OWNER TO postgres;

--
-- TOC entry 3568 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN t_recommendation.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_recommendation.id IS 'id';


--
-- TOC entry 3569 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN t_recommendation.rubric; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_recommendation.rubric IS 'Рубрика';


--
-- TOC entry 3570 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN t_recommendation.caption; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_recommendation.caption IS 'Название';


--
-- TOC entry 3571 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN t_recommendation.description; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_recommendation.description IS 'Описание';


--
-- TOC entry 3572 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN t_recommendation.link; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_recommendation.link IS 'Ссылка';


--
-- TOC entry 185 (class 1259 OID 27815)
-- Name: t_today; Type: TABLE; Schema: library; Owner: postgres
--

CREATE TABLE t_today (
  id bigint NOT NULL,
  date_of date NOT NULL,
  title character varying(100) NOT NULL
);
ALTER TABLE ONLY t_today ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_today ALTER COLUMN date_of SET STATISTICS 0;
ALTER TABLE ONLY t_today ALTER COLUMN title SET STATISTICS 0;


ALTER TABLE t_today OWNER TO postgres;

--
-- TOC entry 3573 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN t_today.id; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_today.id IS 'id';


--
-- TOC entry 3574 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN t_today.date_of; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_today.date_of IS 'Дата памятной даты';


--
-- TOC entry 3575 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN t_today.title; Type: COMMENT; Schema: library; Owner: postgres
--

COMMENT ON COLUMN t_today.title IS 'Заголовок';


--
-- TOC entry 202 (class 1259 OID 28045)
-- Name: v_about_city; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_about_city AS
  SELECT t_about_city.id,
    t_about_city.caption,
    t_about_city."when"
  FROM t_about_city;


ALTER TABLE v_about_city OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 28198)
-- Name: v_adress; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_adress AS
  SELECT t_adress.id,
    t_adress.adress,
    t_adress.phone,
    t_adress.email,
    t_adress.site,
    t_adress.telegram,
    t_adress.watsapp,
    t_adress.vk
  FROM t_adress;


ALTER TABLE v_adress OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 28049)
-- Name: v_affiche; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_affiche AS
  SELECT t_affiche.id,
    t_affiche.date_of,
    t_affiche.content
  FROM t_affiche;


ALTER TABLE v_affiche OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 28325)
-- Name: v_album; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_album AS
  SELECT t_album.id,
    t_album.date_of,
    t_album.title,
    t_album.cover
  FROM t_album;


ALTER TABLE v_album OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 28260)
-- Name: v_article; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_article AS
  SELECT t_article.id,
    t_article.date_of,
    t_article.title,
    t_article.preview_annotation,
    t_article.content,
    t_article.preview_image,
    t_article.content_image,
    t_article.author
  FROM t_article;


ALTER TABLE v_article OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 28202)
-- Name: v_club; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_club AS
  SELECT t_club.id,
    t_club.caption,
    t_club.description,
    t_club.dates,
    t_club.age
  FROM t_club;


ALTER TABLE v_club OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 28206)
-- Name: v_competition; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_competition AS
  SELECT t_competition.id,
    t_competition.caption,
    t_competition.description,
    t_competition.dates,
    t_competition.age
  FROM t_competition;


ALTER TABLE v_competition OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 28214)
-- Name: v_document; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_document AS
  SELECT t_document.id,
    t_document.caption,
    t_document.link
  FROM t_document;


ALTER TABLE v_document OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 28140)
-- Name: v_flashbook; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_flashbook AS
  SELECT t_flashbook.id,
    t_flashbook.caption,
    t_flashbook.link,
    t_flashbook.img
  FROM t_flashbook;


ALTER TABLE v_flashbook OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 28345)
-- Name: v_guest_book; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_guest_book AS
  SELECT t_guest_book.id,
    t_guest_book.author,
    t_guest_book.content,
    t_guest_book.email,
    t_guest_book.date_of
  FROM t_guest_book;


ALTER TABLE v_guest_book OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 28069)
-- Name: v_log; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_log AS
  SELECT t_log.id,
    t_log."user",
    t_log.role,
    t_log.action,
    t_log."table",
    t_log.object,
    t_log.it_was,
    t_log.it_now,
    t_log.sql_revert,
    t_log.timed
  FROM t_log;


ALTER TABLE v_log OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 28073)
-- Name: v_material; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_material AS
  SELECT t_material.id,
    t_material.caption,
    t_material.quantity
  FROM t_material;


ALTER TABLE v_material OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 28194)
-- Name: v_mode; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_mode AS
  SELECT t_mode.id,
    t_mode.holiday,
    t_mode.working,
    t_mode.lunch,
    t_mode.last_day,
    t_mode.season
  FROM t_mode;


ALTER TABLE v_mode OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 28248)
-- Name: v_periodic; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_periodic AS
  SELECT t_periodic.id,
    t_periodic.caption,
    t_periodic.period,
    t_periodic.periodic_type,
    t_periodic_type.caption AS periodic_type_caption,
    t_periodic.language
  FROM (t_periodic
    JOIN t_periodic_type ON ((t_periodic.periodic_type = t_periodic_type.id)));


ALTER TABLE v_periodic OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 28081)
-- Name: v_periodic_type; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_periodic_type AS
  SELECT t_periodic_type.id,
    t_periodic_type.caption
  FROM t_periodic_type;


ALTER TABLE v_periodic_type OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 28311)
-- Name: v_photo; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_photo AS
  SELECT t_photo.id,
    t_photo.caption,
    t_photo.link,
    t_photo.pid
  FROM t_photo;


ALTER TABLE v_photo OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 28252)
-- Name: v_pricelist; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_pricelist AS
  SELECT t_pricelist.id,
    t_pricelist.caption,
    t_pricelist.pid,
    t_pricelist.cost,
    t_pricelist.per_one
  FROM t_pricelist;


ALTER TABLE v_pricelist OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 28210)
-- Name: v_recommendation; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_recommendation AS
  SELECT t_recommendation.id,
    t_recommendation.rubric,
    t_recommendation.caption,
    t_recommendation.description,
    t_recommendation.link
  FROM t_recommendation;


ALTER TABLE v_recommendation OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 28089)
-- Name: v_today; Type: VIEW; Schema: library; Owner: postgres
--

CREATE VIEW v_today AS
  SELECT t_today.id,
    t_today.date_of,
    t_today.title
  FROM t_today;


ALTER TABLE v_today OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- TOC entry 225 (class 1259 OID 28294)
-- Name: v_user_code; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE v_user_code (
  code character varying(30)
);


ALTER TABLE v_user_code OWNER TO postgres;

SET search_path = core, pg_catalog;

--
-- TOC entry 3576 (class 0 OID 0)
-- Dependencies: 201
-- Name: main_seq; Type: SEQUENCE SET; Schema: core; Owner: postgres
--

SELECT pg_catalog.setval('main_seq', 346, true);


--
-- TOC entry 3432 (class 0 OID 27905)
-- Dependencies: 197
-- Data for Name: t_action; Type: TABLE DATA; Schema: core; Owner: postgres
--

COPY t_action (id, caption, code, "table", action) FROM stdin;
3	Доступность админки	adminView	admin	adminView
\.


--
-- TOC entry 3435 (class 0 OID 27928)
-- Dependencies: 200
-- Data for Name: t_log; Type: TABLE DATA; Schema: core; Owner: postgres
--

COPY t_log (id, "user", role, action, "table", object, it_was, it_now, sql_revert, timed) FROM stdin;
\.


--
-- TOC entry 3431 (class 0 OID 27898)
-- Dependencies: 196
-- Data for Name: t_role; Type: TABLE DATA; Schema: core; Owner: postgres
--

COPY t_role (id, caption, code, active) FROM stdin;
2	Обычная	adminViewer	t
\.


--
-- TOC entry 3433 (class 0 OID 27912)
-- Dependencies: 198
-- Data for Name: t_role_action; Type: TABLE DATA; Schema: core; Owner: postgres
--

COPY t_role_action (id, role, action) FROM stdin;
3	adminViewer	adminView
\.


--
-- TOC entry 3430 (class 0 OID 27883)
-- Dependencies: 195
-- Data for Name: t_user; Type: TABLE DATA; Schema: core; Owner: postgres
--

COPY t_user (id, caption, code, login, pass, active) FROM stdin;
108	Библиотека-филиал №30	zelbib30_30	zelbib30	pushkin-lomonosov	t
\.


--
-- TOC entry 3434 (class 0 OID 27920)
-- Dependencies: 199
-- Data for Name: t_user_role; Type: TABLE DATA; Schema: core; Owner: postgres
--

COPY t_user_role (id, "user", role, active) FROM stdin;
4	zelbib30_30	adminViewer	t
\.


SET search_path = library, pg_catalog;

--
-- TOC entry 3429 (class 0 OID 27875)
-- Dependencies: 194
-- Data for Name: t_about_city; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_about_city (id, caption, "when") FROM stdin;
\.


--
-- TOC entry 3438 (class 0 OID 28154)
-- Dependencies: 210
-- Data for Name: t_adress; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_adress (id, adress, phone, email, site, telegram, watsapp, vk) FROM stdin;
77	г.Зеленодольск, ул.Ленина 46	+7(84371) 4-27-17	zel30.bibl@yandex.ru	zelbib.ru
\.


--
-- TOC entry 3422 (class 0 OID 27828)
-- Dependencies: 187
-- Data for Name: t_affiche; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_affiche (id, date_of, content) FROM stdin;
101	2016-03-23 00:00:00	Встреча, посвященная А. Шамову
102	2016-03-17 00:00:00	110 лет со дня рождения К. И. Шульженко
100	2016-04-26 00:00:00	Праздник поэзии "Читая Тукая
\.


--
-- TOC entry 3418 (class 0 OID 27802)
-- Dependencies: 183
-- Data for Name: t_album; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_album (id, date_of, title, cover) FROM stdin;
311	2015-01-01	new best album	http://www.sunhome.ru/i/wallpapers/61/pozitivnie-kartinki.orig.jpg
320	2017-05-03	sss	http://www.sunhome.ru/i/cards/205/vesna-otkritka-kartinka.orig.jpg
\.


--
-- TOC entry 3419 (class 0 OID 27807)
-- Dependencies: 184
-- Data for Name: t_article; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_article (id, date_of, title, preview_annotation, content, preview_image, content_image, author) FROM stdin;
111	2015-12-01 04:05:06	История библиотеки-филиала №30 г. Зеленодольска.	История библиотеки-филиала №30 г. Зеленодольска.	История библиотки:\r\n\r\nв 1909 году при Паратских мастерских была построена церковно-приходская школа, где открылась библиотека. Книжный фонд образовался из пожертвований частных лиц, и также из книг, переданных в дар неким купцом. Работали в библиотеке главным образом учителя. Фонд пополнялся от случая к случаю. В 1919г. В поселке Паратского Затона в одном из бараков был открыт первый клуб, куда перекочевала и библиотека. В 1932г. Профсоюзная библиотека переехала в новое здание Клуба им. Парижской Коммуны и работала до 1967г., пока клуб не сгорел. С 1967-1971 гг. библиотека ютилась в разных помещениях. В 1972г. Библиотека распахнула двери в новом дворце культуры им. А. М. Горького, где находится и по сей день. В 1997г. Была передана в городской отдел культуры в ЦБС г. Зеленодольска. И сейчас является одним из филиалов ЦБС. С 2014г. После ремонта стала модельной библиотекой, с мультимедийными и другими информационными технологиями и возможностями. \r\n\r\nИстория библиотеки дк. им. А. М. Горького связана с историей нашего города, с историей завода им. А.М. Горького. В 1904г. Министерство путей сообщения обратилось в государственный совет с просьбой выделить 130-150 тысяч рублей для расширения Паратских казенных мастерских. Просьба была удовлетворена, и к 1913г. Мастерские превратились в хорошо оснащенную необходимым оборудованием базу для ремонта судов.\r\n\r\nПри мастерских в 1909г была построена церковно-приходская школа, где открылась и библиотека. Книжный фонд ее образовался из пожертвований частных лиц, и также из книг, переданных в дар неким купцом.\r\nРаботали в библиотеке главным образом учителя. Пополнялся фонд от случая к случаю.\r\n\r\nПосле октябрьской революции в марте 1919г в поселке Паратского затона в одном из бараков был открыт первый клуб, сюда перекочевала и библиотека.\r\nВ 1928г. Началось строительство клуба им. Парижской коммуны с большим зрительным залом, с множеством комнат для кружков и оригинальным балконом. С западной стороны в пристрое находилась библиотека. Первыми заведующими библиотекой были Галина Николаевна Шахова, затем Бибинур Садыковна Юльметьева. Здесь проходили литературно-музыкальные вечера. В них принимали участие писатели и поэты, такие как атхи Бурнаш, Муса Джалиль и другие. Книжный фонд библиотеки тогда составлял около 20 тысяч экземпляров книг, из них на татарском языке 5000 экземпляров.\r\n\r\nВ 1967 году Клуба им. Парижской коммуны не стало - он сгорел. Пострадала и библиотека. Сгорело множество книг, но большую часть все же удалось спасти.\r\nДальнейшее развитие, переезд в другое здание, после того как сгорел клуб, связано с именем М. М. Садыковой. Мария Михайловна - человек энергичный и деятельный, посвятившая библиотечному делу 35 лет.\r\n\r\nФонд библиотеки был в основном спасен и перевезен в подростковый клуб «Парус» по адресу ул. Туктарова. Фонд на тот момент составлял 37 000 экземпляров.\r\nБыли сколочены деревянные топорные стеллажи. В этом помещении фонд содержать было нельзя. Было очень сыро, прыгали лягушки, со стен текло. В это время уже строился новый дворец культуры. После длительного ходатайства в профкоме завода и горисполкоме было выделено другое помещение на углу ул. Гоголя и Туктара. Оно было очень маленькое, всего 25 кв. м. Часть фонда хранилась в связанном виде.\r\n\r\nУ библиотеки был еще филиал на улице Фабричной, где обслуживались жители этого микрорайона и учащиеся школы и ПТУ-25. Кроме того в заводе работало более 15 передвижек.\r\n\r\nШтат библиотеки состоял из 3-ех человек. Заведующая, работник филиала - Гарифуллина Ф, заведующая абонементом Парсина Т. Позднее выделяют единицу заведующего передвижным фондом. На этой должности работала Кулагина Н. В.\r\nВ 1972г. xитатели получили отличный подарок - библиотеку в новом дворце культуры. К тому времени в книжном фонде библиотеки насчитывалось около 40 тысяч томов. К услугам посетителей был предоставлен прекрасный читальный зал, абонемент с открытым доступом, добротное книгохранилище. Все было обставлено отличной мебелью по специальному заказу.\r\n\r\nВстал вопрос о выделении помещения для обслуживания детей. Было выделено помещение 12 кв. м., а в 1979 г. выхлопотали помещение на втором этаже дк в 30 кв м., где она расположена и по сей день. Первой зав. детским отделением была назначена Коклихина Р. Н., затем Силантьева Е. И.\r\nВ середине 70-х гг. в библиотеку приходит новое поколение молодых библиотекарей: Сагдиева Р. А., Сафина Ф. Г., Серякова Т. К., Гурьянова Л. А.\r\n\r\nБиблиотека в дк горького расширялась, менялась. Работники библиотеки не ограничивались работой только в стенах библиотеки. У библиотеки был еще филиал на улице фабричной, где обслуживались жители этого микрорайона и учащиеся школы и ПТУ-25. Был открыт пункт выдачи на ул. Заикина (в общежитии «Светлана»). Зав. Филиалом была назначена Серякова Т. К.\r\n\r\nВ цехах завода, детских садах, была большая сеть передвижек, а позднее открыт филиал библиотеки в комплексе завода с фондом 10 тысяч экземпляров, где обслуживались передвижки и индивидуальные читатели. Зав. отделом была назначена Сафина Ф. Г.\r\n\r\nВо дворце культуры им. Горького проходили различные массовые мероприятия, в них активное участие принимала и библиотека.\r\n\r\nВ 1986 г. Заведующей библиотекой дк им А. М. Горького назначена Сагдиева Роза Абдуловна. Свою трудовую деятельность она начала в 1974 г. В детской библиотеке на ул. Мичурина. В 1976 г. Перешла работать в библиотеку дк им горького и в течение 10 лет работала на одном из ответственных участков-абонементе. В 1978 г. Заочно окончила Казанский государственный институт культуры.\r\n\r\nРоза Абдуловна-достойный преемник Садыковой М. М. Профессионализм, широкая эрудиция, любовь к книге и чтению, уважение к людям, умение находить выход из сложных ситуаций - это те качества, которые присущи ей как руководителю. Сагдиева Р. А. возглавляет коллектив и по сей день.\r\n\r\nВо время ее работы сформировался дружный творческий коллектив. Коллектив пополнился новыми работниками: Ситниковой Л. А (абонемент) и Лазаревой О. А. (детское отделение).\r\n\r\n	../../../assets/img/parts/news/2015-12-01/middleimage.jpg	../../../assets/img/parts/news/2015-10-26/img_for_content.jpg	zelbib30_30
113	2016-04-14 04:05:06	Праздник поэзии у городского озера "Читая Тукая"	Праздник поэзии у городского озера. К участию приглашаются все желающие.	\r\n26 апреля. Праздник поэзии у городского озера "Читая Тукая". К участию приглашаются все желающие!\r\n	../../../assets/img/parts/news/2016-04-14/middleimage.jpg	../../../assets/img/parts/news/2016-04-14/img_for_content.jpg	zelbib30_30
114	2016-03-16 04:05:06	110 лет со дня рождения А. Барто	Литературный праздник "Юбилей писателя - праздник для читателя"	\r\nМероприятие проведено в рамках проекта "Культурный дневник школьника". Гимназия №3, 1В класс (30 человек). Книжная выставка: "Мир, в котором живут дети". Литературный праздник "Юбилей писателя - праздник для читателя".\r\n	../../../assets/img/parts/news/2016-03-16/middleimage.jpg	../../../assets/img/parts/news/2016-03-16/img_for_content.jpg	zelbib30_30
115	2016-02-12 04:05:06	Габдулла Тукай: великий поэт.	В этом году исполняется 130 лет со дня рождения великого татарского поэта Г. Тукая.	\r\n“Я проснулся, проснулся, чтоб не засыпать вовек”, - написал великий поэт Габдулла Тукай, находясь в больнице на смертном одре. И оказался пророком. За недолгую творческую жизнь писатель успел стать символом татарского народа. Его произведения актуальны всегда, потому что в них он обращается к вечным ценностям. Габдулла Тукай пишет о патриотизме и любви к людям. В честь 130-летнего юбилея творца компания «Таттелеком» совместно с библиотекой-филиалом №30 организовала литературные чтения. Одиннадцатого февраля в МБУ «ЦБС ЗМР» библиотека-филиал №30, расположенной в ЦКИНТ им. М.Горького, прошла встреча работников и ветеранов компании «Таттелеком», их детей и внуков с учащимися гимназии № 3, посвященная творчеству писателя. С вступительной речью выступил начальник Зеленодольского РОЭС Казанского управления электрической связи по ПАО «Таттелеком» Носков Евгений Александрович. Он отметил высокое значение поэзии Г. Тукая для всех поколений читателей, особенно для молодежи. Литературно-музыкальную композицию «Стихи народу посвящаю…» вела библиотекарь Ахметова Лилия Шавкатовна. Она познакомила гостей с фактами из биографии поэта. Рассказ сопровождался музыкальными клипами и видео фрагментами. Вечер украсили музыкальные номера: «Ноктюрн» на флейте исполнила учащаяся гимназии №3 Айгуль, а сотрудница компании «Таттелеком» Ахметова Рушания Рафиковна исполнила песню «Туган авыл». Ученики гимназии № 3 прочитали свои любимые стихотворения. На звучную татарскую речь даже пришел один из самых интересных персонажей сказок, Шурале. Мифического героя исполнил библиотекарь Каштанов Антон Сергеевич. В конце мероприятия выступил ведущий инженер Зеленодольского РОЭС Касимов Рафаэль Ильгизович вручил призы и грамоты активным участникам мероприятия. Гости весело провели время, реализовали свои таланты и вместе ещё раз насладились произведениями национального поэта. \r\nКаролина Фасхиева.\r\n	../../../assets/img/parts/news/2016-02-12/middleimage.jpg	../../../assets/img/parts/news/2016-03-16/img_for_content.jpg	zelbib30_30
116	2015-12-09 04:05:06	Героизм может быть родным.	Встреча с ветеранами Афганской войны (1979—1989). Встреча приурочена Дню Героев Отечества. 	\r\nГероизм может быть родным. 09.12.15 в стенах библиотеки-филиала №30 прошла встреча с ветеранами Афганской войны (1979—1989). Встреча приурочена Дню Героев Отечества. \r\nЭтот праздник, день памяти, отмечается в нашей стране с 2007 года. Принят он был 25 января 2007 года. Задумка принятия была такова: Борис Вячеславович Грызлов, Председатель Высшего совета партии «Единая Россия», выдвинул законопроект о восстановлении этого дня, пояснив : « Речь идет о восстановлении существовавшего в дореволюционной России праздника — Дня георгиевских кавалеров, который отмечался 9 декабря. Эта же дата будет закреплена и за Днем героев Отечества, которые достойны иметь свой праздник». 21 февраля 2007 года инициатива депутатов было одобрена Советом Федерации. 28 февраля 2007 года её утвердил Президент Российской Федерации Владимир Владимирович Путин. День Героев Отечества отмечался еще в дореволюционной России. Отмечался он как День георгиевских кавалеров. Именно 9 декабря 1769 года императрицей Екатериной II был учрежден Императорский Военный орден Святого Великомученика и Победоносца Георгия, что и послужило появлению Дня георгиевских кавалеров. \r\nНа встрече ветераны делились своими воспоминаниями, мыслями о войне, планами на будущее. Ученики 9-ых классов 1-го лицея тепло встретили героев. Особенно учеников заинтересовали полученные награды, солдатский жаргон, боевые операции и быт войны. Посетили библиотеку Бинчуров Камиль Джамилевич, командир разведывательной группы спецподразделения разведки ГРУ и Хасанов Руслан Разимович , летчик, Герой Афганской войны. Также мероприятие посетили 31 учащийся.\r\nВ рамках мероприятия школьников познакомили с фильмами-хрониками об Афганской войне. Были освещены история, этапы и результаты войны. Презентация раскрыла ключевые моменты этого периода времени.\r\nСотрудники библиотеки представили книжную выставку, посвященную Героям Отечества. Лилия Шавкатовна познакомила учеников с биографиями и судьбами Героев России. \r\nРаботники библиотеки выражают благодарность участникам мероприятия и желают всем здоровья и успехов.\r\n	../../../assets/img/parts/news/2015-12-09/middleimage.jpg	../../../assets/img/parts/news/2015-12-09/img_for_content.jpg	zelbib30_30
112	2015-10-22 00:00:00	Запуск первой рабочей версии сайта библиотеки.	Запуск первой рабочей стабильной версии сайта библиотеки!	\r\nПоздравляем с запуском первой стабильной версии сайта библиотеки! Впереди нас ожидают развитие и продвижение!\r\n\r\nВашему вниманию представлены новости библиотеки, пополняющийся фотоальбом, списки предоставляемых услуг и их стоимость, информация о готовящихся конкурсах и мероприятиях. Также можно посмотреть какие клубы на сегодняшний момент имеют место быть в нашей библиотеке. Помимо всего прочего, мы ведем и пополняем справку об истории и развитии библиотеки. Если Вам интересна эта информация, можете с ней ознакомиться в разделе "О библиотеке-История библиотеки". Также в активной разработке находится рекомендательный раздел "Списки литературы". В нем можно ознакоиться с рекомендациями по чтению разных жанров по разеым возрастам. Также мы готовим краеведческую справку о городе Зеленодольске. Ознакомиться с ней можно будет в ближайшее время во вкладке "О городе". \r\nПомимо публикаций на сайте библиотеки, информацию о нас можно найти на наших страничках в соц. сетях. \r\nБиблиотека активно учавствует в мировом книгообмене "Букроссинг". Информацию об отпущенных на волю книгах можно посмотреть на нашей полке на bookrossing.ru \r\nТакже мы учавствуем в библиотечной акции "Флешбук".Наши странички можно посмотреть вконтакте. \r\nМы ведем работу по созданию буктрейлеров. Информацию о них можно найти по ссылке буктрейлеров на главной страничке нашего сайта. \r\nПосле полной индексации сайта поисковыми системами можно будет пользоваться поиском по сайту. Это ускорит Вашу работу с нашим сайтом. \r\nСвои предложения, жалобы и идеи Вы можете присылать нам на почту либо оставить сообщение в гостевой книге со своими координатами. Ни одно сообщение не останется без ответа. \r\n\r\nС уважением, работники библиотеки-филиала №30.\r\n	../../../assets/img/parts/news/2015-10-22/middleimage.jpg	../../../assets/img/parts/news/2015-10-22/img_for_content.jpg	zelbib30_30
\.


--
-- TOC entry 3439 (class 0 OID 28162)
-- Dependencies: 211
-- Data for Name: t_club; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_club (id, caption, description, dates, age) FROM stdin;
117	Профориентационный клуб "Ориентир"	Клуб знакомит читателей и гостей библиотеки с различными профессиями и ремеслами.	Встречи проходят в плановом порядке.	от 15 и старше
118	«Юный эрудит» (разной направленности)	Детско-подростковый клуб, направленный на развитие и процветание человеческого ума.	Встречи проходят в плановом порядке.	от 7 до 14 лет
\.


--
-- TOC entry 3440 (class 0 OID 28170)
-- Dependencies: 212
-- Data for Name: t_competition; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_competition (id, caption, description, dates, age) FROM stdin;
119	Конкурс на лучший рисунок по книге!	В ближайшее время конкурс будет объявлен.	C 16.05.1992 по 17.06.2020	Среди школьников
120	Лучшее эссе по книге!	В ближайшее время конкурс будет объявлен.	C 16.05.1992 по 17.06.2020	Среди школьников
121	Лучшее сочинение по книге!!!	В ближайшее время конкурс будет объявлен.	C 16.05.1992 по 17.06.2020	Среди школьников
\.


--
-- TOC entry 3442 (class 0 OID 28186)
-- Dependencies: 214
-- Data for Name: t_document; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_document (id, caption, link) FROM stdin;
124	Вкладыш для буккроссинга	https://yadi.sk/i/d7gSCAae3JN9u8
125	Двойной формуляр	https://yadi.sk/i/7o__EIuQ3JN9uA
126	Формуляр-вкладыш	https://yadi.sk/i/yz3j9ZJ03JN9uH
127	Героев славных имена	https://yadi.sk/i/0E5ccxfn3JN9uL
128	Название каталогов	https://yadi.sk/i/Uijdk8QT3JN9uP
\.


--
-- TOC entry 3424 (class 0 OID 27841)
-- Dependencies: 189
-- Data for Name: t_flashbook; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_flashbook (id, caption, link, img) FROM stdin;
106	Башлачев А. С.	http://vk.com/bashlachevpage	../../../assets/img/bashlachev.jpg
107	Шаламов В. Т.	http://vk.com/shalamovpage	../../../assets/img/shalamov.jpg
332	Машнин А.	https://vk.com/mashninpage	http://1000plastinok.net/artists/previews/3619.jpg
\.


--
-- TOC entry 3421 (class 0 OID 27820)
-- Dependencies: 186
-- Data for Name: t_guest_book; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_guest_book (id, author, content, email, date_of) FROM stdin;
129	Артем	Спасибо за помощь в поиске информации. Был приятно удивлен количеством энциклопедий.	samolenko@gmail.com	\N
130	Андрей Николаевич	С удовольствием хожу в Вашу библиотеку! Спасибо!	example@mail.com	\N
131	Антон	Рад, что Вам нравится,приходите чаще!	kashton1992@ya.ru	\N
\.


--
-- TOC entry 3423 (class 0 OID 27833)
-- Dependencies: 188
-- Data for Name: t_log; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_log (id, "user", role, action, "table", object, it_was, it_now, sql_revert, timed) FROM stdin;
\.


--
-- TOC entry 3428 (class 0 OID 27867)
-- Dependencies: 193
-- Data for Name: t_material; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_material (id, caption, quantity) FROM stdin;
78	Компьютеры с выходом в internet	8
79	Проэктор и экран(VGA и HDMI соединения)	1
80	Широкоформатный плазменный монитор	3
81	Беспроводной микрофон	1
82	Конференц-станция: беспроводные микрофоны	8
83	Беспроводные наушники	6
84	Проигрыватель виниловых дисков	3
85	Сканер	1
86	Принтер	3
87	Декодеры оцифровки:винил,диски,кассеты	2
88	Конференц зал на ~30 человек
\.


--
-- TOC entry 3437 (class 0 OID 28149)
-- Dependencies: 209
-- Data for Name: t_mode; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_mode (id, holiday, working, lunch, last_day, season) FROM stdin;
76	Суббота, Воскресенье	10.00-18.00	12.00-13.00	санитарный	winter
75	Суббота	10.00-18.00	12.00-13.00	санитарный	summer
\.


--
-- TOC entry 3426 (class 0 OID 27857)
-- Dependencies: 191
-- Data for Name: t_periodic; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_periodic (id, caption, period, periodic_type, language) FROM stdin;
6	Вечерняя Казань		4	русский
7	Советский спорт.		4	русский
8	Аргументы и факты.		4	русский
9	Ватаным Татарстан.		4	русский
10	Веста.		4	русский
11	Все для женщины.		4	русский
12	Зеленодольская правда.		4	русский
13	Зеленый дол.		4	русский
14	ЗОЖ.		4	русский
15	Комсомольская правда.		4	русский
16	Мәдәни җомга.		4	татарский
17	Республика Татарстан.		4	русский
18	Сам хозяин.		4	русский
19	Татарстан Яшьләре.		4	русский
20	Шәһри Казан.		4	русский
21	Юлдаш.		4	русский
22	Яшел Үзән.		4	русский
23	Аргамак.		5	русский
24	Библиотека.		5	русский
25	Бурда моден.		5	русский
26	Виноград.		5	русский
27	Винни и его друзья.		5	русский
28	Вокруг света.		5	русский
29	Гасырлар авазы.		5	русский
30	Девчонки-мальчишки.		5	русский
31	Детская энциклопедия.		5	русский
32	Друг кошек.		5	русский
33	Здоровье.		5	русский
34	Идел.		5	русский
35	Идель.		5	русский
36	Казан Утлары.		5	русский
37	Казань.		5	русский
38	Караван историй.		5	русский
39	Книжки, нотки.		5	русский
40	Коллекция идей.		5	русский
41	Майдан.		5	русский
42	Миша.		5	русский
43	Мой уютный дом.		5	русский
44	Мурзилка.		5	русский
45	Мы.		5	русский
46	Мәгариф.		5	русский
47	Наука и жизнь.		5	русский
48	Наука и религия.		5	русский
49	Незнайка.		5	русский
51	Природа и человек.		5	русский
52	Приусадебное хозяйство.		5	русский
53	Простоквашино.		5	русский
54	Путеводная звезда.		5	русский
55	Ромео и Джульета.		5	русский
56	Сабантуй.		5	русский
57	Салават купере.		5	русский
58	Сандра.		5	русский
59	Смена.		5	русский
60	Сөембикә.		5	русский
61	Сәхнә.		5	русский
62	Татарстан(рус).		5	русский
63	Татарстан(тат).		5	русский
64	Техника молодежи.		5	русский
65	Тошка.		5	русский
66	Тугэрэк уен.		5	русский
67	Физкультура и Спорт.		5	русский
68	Хэллоу!		5	русский
69	Чаян(рус).		5	русский
70	Читаем, учимся, играем.		5	русский
71	Чудеса и приключения.		5	русский
72	Чудеса и Приключения Детям.		5	русский
73	Школьный вестник.		5	русский
74	Ялкын.		5	русский
191	Подвиг	Раз в месяц	4	Русский
\.


--
-- TOC entry 3427 (class 0 OID 27862)
-- Dependencies: 192
-- Data for Name: t_periodic_type; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_periodic_type (id, caption) FROM stdin;
4	Газеты
5	Журналы
\.


--
-- TOC entry 3444 (class 0 OID 28297)
-- Dependencies: 226
-- Data for Name: t_photo; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_photo (id, caption, link, pid) FROM stdin;
312	buubi	http://sc2.blizzwiki.ru/images/7/74/Kerrigan.jpg	311
313	buubia	https://i.ytimg.com/vi/z9fJB4pZlKA/maxresdefault.jpg	311
314	buussbia	https://upload.wikimedia.org/wikipedia/ru/thumb/4/4c/Sarah_Kerrigan_Broodwar.jpg/250px-Sarah_Kerrigan_Broodwar.jpg	311
315	buussssbia	http://media.blizzard.com/sc2/media/artwork/cin_kerrigan_hydra-large.jpg	311
316	buussssssbia	http://mmo.one/upload/medialibrary/316/kerrigan.jpg	311
318	sxsx	http://www.sunhome.ru/i/wallpapers/31/gomer-simpson-kartinka.orig.jpg	311
321	sss	http://www.sunhome.ru/UsersGallery/Cards/prazdnik_den_zemli_kartinka.jpg	320
322	sssscs	http://minionomaniya.ru/wp-content/uploads/2016/01/%D0%BC%D0%B8%D0%BD%D1%8C%D0%BE%D0%BD%D1%8B-%D0%BF%D1%80%D0%B8%D0%BA%D0%BE%D0%BB%D1%8B-%D0%BA%D0%B0%D1%80%D1%82%D0%B8%D0%BD%D0%BA%D0%B8.jpg	320
323	sssscs	http://www.cruzo.net/user/images/k/d760fbd8f50d9b92dc054ee8390df166_617.jpg	320
324	sssscscs	http://www.cruzo.net/user/images/k/ecc3ecf42c75db1ffce5d06cbe95b1e6_644.jpg	320
325	sssscscsc	http://minionomaniya.ru/wp-content/uploads/2015/09/%D0%9A%D0%B0%D1%80%D1%82%D0%B8%D0%BD%D0%BA%D0%B8-%D0%BC%D0%B8%D0%BD%D1%8C%D0%BE%D0%BD%D0%BE%D0%B2.jpg	320
326	sssscscscs	http://artemdemo.me/blog/wp-content/uploads/2014/09/angularjs.jpg	320
327	sssscscscss	https://cdn.worldvectorlogo.com/logos/angular-icon-1.svg	320
328	sssscscscsscs	http://codecondo.com/wp-content/uploads/2015/05/15-Directives-to-Extend-Your-Angular.js-Apps.jpg?x94435	320
330	sssscscscsscs	https://yellowpencil.com/assets/blog/banners/banner-angularjs.jpg	311
331	sssscscscsscs	https://d2eip9sf3oo6c2.cloudfront.net/series/square_covers/000/000/033/thumb/egghead-angular-material-course-sq.png?1460301246	320
\.


--
-- TOC entry 3425 (class 0 OID 27849)
-- Dependencies: 190
-- Data for Name: t_pricelist; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_pricelist (id, caption, pid, cost, per_one) FROM stdin;
89	Плата за формуляр при регистрации: взрослые/пенсионеры/дети	\N	30/25/25	Год
90	Оцифровка аудиокассет	\N	20	Кассета
91	Оцифровка видеокассет	\N	150	Кассета
92	Самостоятельная работа на компьютере: взрослые/дети	\N	25/20	Час
94	Сканирование документа	\N	6	Лист
95	Ксерокопия документа	\N	4	Проход
98	Возмещение за порчу и утерю книг	\N	Стоимость книги	\N
99	Взимание пени за нарушение сроков пользования литературой	\N	0.30	Книга/день
93	Печать и набор текстa (предварительная оплата 50%)	\N	20	Лист
\.


--
-- TOC entry 3441 (class 0 OID 28178)
-- Dependencies: 213
-- Data for Name: t_recommendation; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_recommendation (id, rubric, caption, description, link) FROM stdin;
123	Фантастика	Получившие премию Ньюбелла и Хьюга	Получившие премию Ньюбелла и Хьюга	https://en.wikipedia.org/wiki/List_of_joint_winners_of_the_Hugo_and_Nebula_awards
\.


--
-- TOC entry 3420 (class 0 OID 27815)
-- Dependencies: 185
-- Data for Name: t_today; Type: TABLE DATA; Schema: library; Owner: postgres
--

COPY t_today (id, date_of, title) FROM stdin;
104	2017-05-22	День рождения Вилема Завады
105	2017-05-21	День военного переводчика
\.


SET search_path = public, pg_catalog;

--
-- TOC entry 3443 (class 0 OID 28294)
-- Dependencies: 225
-- Data for Name: v_user_code; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY v_user_code (code) FROM stdin;
zelbib30_30
\.


SET search_path = core, pg_catalog;

--
-- TOC entry 3247 (class 2606 OID 27911)
-- Name: t_action_code_key; Type: CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_action
    ADD CONSTRAINT t_action_code_key UNIQUE (code);


--
-- TOC entry 3249 (class 2606 OID 27909)
-- Name: t_action_pkey; Type: CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_action
    ADD CONSTRAINT t_action_pkey PRIMARY KEY (id);


--
-- TOC entry 3255 (class 2606 OID 27935)
-- Name: t_log_pkey; Type: CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_log
    ADD CONSTRAINT t_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3251 (class 2606 OID 27919)
-- Name: t_role_action_pkey; Type: CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_role_action
    ADD CONSTRAINT t_role_action_pkey PRIMARY KEY (id);


--
-- TOC entry 3243 (class 2606 OID 27904)
-- Name: t_role_code_key; Type: CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_role
    ADD CONSTRAINT t_role_code_key UNIQUE (code);


--
-- TOC entry 3245 (class 2606 OID 27902)
-- Name: t_role_pkey; Type: CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_role
    ADD CONSTRAINT t_role_pkey PRIMARY KEY (id);


--
-- TOC entry 3235 (class 2606 OID 27892)
-- Name: t_user_caption_key; Type: CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_user
    ADD CONSTRAINT t_user_caption_key UNIQUE (caption);


--
-- TOC entry 3237 (class 2606 OID 27894)
-- Name: t_user_code_key; Type: CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_user
    ADD CONSTRAINT t_user_code_key UNIQUE (code);


--
-- TOC entry 3239 (class 2606 OID 27896)
-- Name: t_user_login_key; Type: CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_user
    ADD CONSTRAINT t_user_login_key UNIQUE (login);


--
-- TOC entry 3241 (class 2606 OID 27890)
-- Name: t_user_pkey; Type: CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_user
    ADD CONSTRAINT t_user_pkey PRIMARY KEY (id);


--
-- TOC entry 3253 (class 2606 OID 27927)
-- Name: t_user_role_pkey; Type: CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_user_role
    ADD CONSTRAINT t_user_role_pkey PRIMARY KEY (id);


SET search_path = library, pg_catalog;

--
-- TOC entry 3233 (class 2606 OID 27882)
-- Name: t_about_city_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_about_city
    ADD CONSTRAINT t_about_city_pkey PRIMARY KEY (id);


--
-- TOC entry 3259 (class 2606 OID 28161)
-- Name: t_adress_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_adress
    ADD CONSTRAINT t_adress_pkey PRIMARY KEY (id);


--
-- TOC entry 3217 (class 2606 OID 27832)
-- Name: t_affiche_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_affiche
    ADD CONSTRAINT t_affiche_pkey PRIMARY KEY (id);


--
-- TOC entry 3209 (class 2606 OID 27806)
-- Name: t_album_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_album
    ADD CONSTRAINT t_album_pkey PRIMARY KEY (id);


--
-- TOC entry 3211 (class 2606 OID 27814)
-- Name: t_article_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_article
    ADD CONSTRAINT t_article_pkey PRIMARY KEY (id);


--
-- TOC entry 3261 (class 2606 OID 28169)
-- Name: t_club_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_club
    ADD CONSTRAINT t_club_pkey PRIMARY KEY (id);


--
-- TOC entry 3263 (class 2606 OID 28177)
-- Name: t_competition_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_competition
    ADD CONSTRAINT t_competition_pkey PRIMARY KEY (id);


--
-- TOC entry 3267 (class 2606 OID 28193)
-- Name: t_document_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_document
    ADD CONSTRAINT t_document_pkey PRIMARY KEY (id);


--
-- TOC entry 3221 (class 2606 OID 28139)
-- Name: t_flashbook_img_key; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_flashbook
    ADD CONSTRAINT t_flashbook_img_key UNIQUE (img);


--
-- TOC entry 3223 (class 2606 OID 27848)
-- Name: t_flashbook_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_flashbook
    ADD CONSTRAINT t_flashbook_pkey PRIMARY KEY (id);


--
-- TOC entry 3215 (class 2606 OID 27827)
-- Name: t_guest_book_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_guest_book
    ADD CONSTRAINT t_guest_book_pkey PRIMARY KEY (id);


--
-- TOC entry 3219 (class 2606 OID 27840)
-- Name: t_log_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_log
    ADD CONSTRAINT t_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3231 (class 2606 OID 27874)
-- Name: t_material_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_material
    ADD CONSTRAINT t_material_pkey PRIMARY KEY (id);


--
-- TOC entry 3257 (class 2606 OID 28153)
-- Name: t_mode_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_mode
    ADD CONSTRAINT t_mode_pkey PRIMARY KEY (id);


--
-- TOC entry 3227 (class 2606 OID 27861)
-- Name: t_periodic_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_periodic
    ADD CONSTRAINT t_periodic_pkey PRIMARY KEY (id);


--
-- TOC entry 3229 (class 2606 OID 27866)
-- Name: t_periodic_type_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_periodic_type
    ADD CONSTRAINT t_periodic_type_pkey PRIMARY KEY (id);


--
-- TOC entry 3269 (class 2606 OID 28304)
-- Name: t_photo_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_photo
    ADD CONSTRAINT t_photo_pkey PRIMARY KEY (id);


--
-- TOC entry 3225 (class 2606 OID 27856)
-- Name: t_pricelist_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_pricelist
    ADD CONSTRAINT t_pricelist_pkey PRIMARY KEY (id);


--
-- TOC entry 3265 (class 2606 OID 28185)
-- Name: t_recommendation_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_recommendation
    ADD CONSTRAINT t_recommendation_pkey PRIMARY KEY (id);


--
-- TOC entry 3213 (class 2606 OID 27819)
-- Name: t_today_pkey; Type: CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_today
    ADD CONSTRAINT t_today_pkey PRIMARY KEY (id);


SET search_path = core, pg_catalog;

--
-- TOC entry 3280 (class 2606 OID 28026)
-- Name: t_log_fk; Type: FK CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_log
    ADD CONSTRAINT t_log_fk FOREIGN KEY ("user") REFERENCES t_user(code);


--
-- TOC entry 3281 (class 2606 OID 28031)
-- Name: t_log_fk1; Type: FK CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_log
    ADD CONSTRAINT t_log_fk1 FOREIGN KEY (role) REFERENCES t_role(code);


--
-- TOC entry 3282 (class 2606 OID 28036)
-- Name: t_log_fk2; Type: FK CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_log
    ADD CONSTRAINT t_log_fk2 FOREIGN KEY (action) REFERENCES t_action(code);


--
-- TOC entry 3276 (class 2606 OID 28006)
-- Name: t_role_action_fk; Type: FK CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_role_action
    ADD CONSTRAINT t_role_action_fk FOREIGN KEY (role) REFERENCES t_role(code);


--
-- TOC entry 3277 (class 2606 OID 28011)
-- Name: t_role_action_fk1; Type: FK CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_role_action
    ADD CONSTRAINT t_role_action_fk1 FOREIGN KEY (action) REFERENCES t_action(code);


--
-- TOC entry 3278 (class 2606 OID 28016)
-- Name: t_user_role_fk; Type: FK CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_user_role
    ADD CONSTRAINT t_user_role_fk FOREIGN KEY ("user") REFERENCES t_user(code);


--
-- TOC entry 3279 (class 2606 OID 28289)
-- Name: t_user_role_fk1; Type: FK CONSTRAINT; Schema: core; Owner: postgres
--

ALTER TABLE ONLY t_user_role
    ADD CONSTRAINT t_user_role_fk1 FOREIGN KEY (role) REFERENCES t_role(code);


SET search_path = library, pg_catalog;

--
-- TOC entry 3270 (class 2606 OID 27954)
-- Name: t_article_fk; Type: FK CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_article
    ADD CONSTRAINT t_article_fk FOREIGN KEY (author) REFERENCES core.t_user(code);


--
-- TOC entry 3271 (class 2606 OID 27980)
-- Name: t_log_fk; Type: FK CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_log
    ADD CONSTRAINT t_log_fk FOREIGN KEY ("user") REFERENCES core.t_user(code);


--
-- TOC entry 3272 (class 2606 OID 27986)
-- Name: t_log_fk1; Type: FK CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_log
    ADD CONSTRAINT t_log_fk1 FOREIGN KEY (role) REFERENCES core.t_role(code);


--
-- TOC entry 3273 (class 2606 OID 27991)
-- Name: t_log_fk2; Type: FK CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_log
    ADD CONSTRAINT t_log_fk2 FOREIGN KEY (action) REFERENCES core.t_action(code);


--
-- TOC entry 3275 (class 2606 OID 28001)
-- Name: t_periodic_fk; Type: FK CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_periodic
    ADD CONSTRAINT t_periodic_fk FOREIGN KEY (periodic_type) REFERENCES t_periodic_type(id);


--
-- TOC entry 3283 (class 2606 OID 28306)
-- Name: t_photo_fk; Type: FK CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_photo
    ADD CONSTRAINT t_photo_fk FOREIGN KEY (pid) REFERENCES t_album(id);


--
-- TOC entry 3274 (class 2606 OID 27996)
-- Name: t_pricelist_fk; Type: FK CONSTRAINT; Schema: library; Owner: postgres
--

ALTER TABLE ONLY t_pricelist
    ADD CONSTRAINT t_pricelist_fk FOREIGN KEY (pid) REFERENCES t_pricelist(id);


--
-- TOC entry 3451 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-05-26 17:59:10

--
-- PostgreSQL database dump complete
--

