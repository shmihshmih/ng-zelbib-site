<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");

require_once ('../db.php');

$stmt = $connect->prepare("
  select v.id,
         v.rubric,
         v.caption,
         v.description,
         v.link
  from $db.v_recommendation v
");

$stmt->execute();

while ( $row = $stmt->fetch() ) {
  $data[] = [
    'id' => $row['id'],
    'rubric' => $row['rubric'],
    'caption' => $row['caption'],
    'description' => $row['description'],
    'link' => $row['link']
  ];
}

if ($data) {
  echo json_encode($data);
} else {
  echo json_encode([]);
}

?>