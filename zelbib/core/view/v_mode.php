<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");

require_once ('../db.php');

$stmt = $connect->prepare("
  select v.id,
         v.holiday,
         v.working,
         v.lunch,
         v.last_day,
         v.season
  from $db.v_mode v
");

$stmt->execute();

while ( $row = $stmt->fetch() ) {
  $data[] = [
    'id' => $row['id'],
    'holiday' => $row['holiday'],
    'working' => $row['working'],
    'lunch' => $row['lunch'],
    'last_day' => $row['last_day'],
    'season' => $row['season']
  ];
}

if ($data) {
  echo json_encode($data);
} else {
  echo json_encode([]);
}

?>