<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");

require_once ('../db.php');

$stmt = $connect->prepare("
  select v.id,
         v.date_of::date,
         v.title,
         v.preview_annotation,
         v.content,
         v.preview_image,
         v.content_image,
         v.author
  from $db.v_article v
  order by v.date_of desc
");

$stmt->execute();

while ( $row = $stmt->fetch() ) {
  $data[] = [
    'id' => $row['id'],
    'date_of' => $row['date_of'],
    'title' => $row['title'],
    'preview_annotation' => $row['preview_annotation'],
    'content' => $row['content'],
    'preview_image' => $row['preview_image'],
    'content_image' => $row['content_image'],
    'author' => $row['author']
  ];
}

if ($data) {
  echo json_encode($data);
} else {
  echo json_encode([]);
}

?>