<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");

require_once ('../db.php');

$stmt = $connect->prepare("
  select v.id,
         v.user,
         v.role,
         v.action,
         v.table,
         v.object,
         v.it_was,
         v.it_now,
         v.sql_revert,
         v.timed
  from $db.v_log v
");

$stmt->execute();

while ( $row = $stmt->fetch() ) {
  $data[] = [
    'id' => $row['id'],
    'user' => $row['user'],
    'role' => $row['role'],
    'action' => $row['action'],
    'table' => $row['table'],
    'object' => $row['object'],
    'it_was' => $row['it_was'],
    'it_now' => $row['it_now'],
    'sql_revert' => $row['sql_revert'],
    'timed' => $row['timed']
  ];
}

if ($data) {
  echo json_encode($data);
} else {
  echo json_encode([]);
}

?>