<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");

require_once ('../db.php');

$stmt = $connect->prepare("
  select v.id,
         v.caption,
         v.link,
         v.pid
  from $db.v_photo v
");

$stmt->execute();

while ( $row = $stmt->fetch() ) {
  $data[] = [
    'id'      => $row['id'],
    'caption' => $row['caption'],
    'link'    => $row['link'],
    'pid'     => $row['pid']
  ];
}

if ($data) {
  echo json_encode($data);
} else {
  echo json_encode([]);
}

?>