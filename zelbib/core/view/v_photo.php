<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ('../db.php');
//редактирование или добавление

$decodedData['album']['id'] = $_GET['pid'];
if(isset($decodedData['album'])) {
  $stmt = $connect->prepare("
  select v.id,
         v.caption,
         v.link,
         v.pid
  from $db.v_photo v
  where v.pid = :pid
");

  $stmt->execute(array(
      'pid'       => $decodedData['album']['id'],
    )
  );

  while ( $row = $stmt->fetch() ) {
$datas[] = [
      'preview_xxs' =>
          [
            'path'   => $row['link'],
            'width'  => 500,
            'height' => 375
          ],
    "preview_xs" => [
      "path" => $row['link'],
      "width" => 1024,
      "height" => 768
    ],
    "preview_s"=> [
      "path" => $row['link'],
      "width" => 1440,
      "height" => 1080
    ],
    "preview_m"=> [
      "path" => $row['link'],
      "width" => 2133,
      "height" => 1600
    ],
    "preview_l"=> [
      "path" => $row['link'],
      "width" => 2880,
      "height" => 2160
    ],
    "preview_xl"=> [
      "path" => $row['link'],
      "width" => 3840,
      "height" => 2880
    ],
    "raw"=> [
      "path" => $row['link'],
      "width" => 1400,
      "height" => 1050
    ],
    "dominantColor" => "#a6a6a6"
  ];
  };

  if ($datas) {
    echo json_encode($datas);
  } else {
    echo json_encode($decodedData['albumId']);
  }


}
?>