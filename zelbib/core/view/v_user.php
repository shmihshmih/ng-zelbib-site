<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");

require_once ('../db.php');

$stmt = $connect->prepare("
  select v.id,
         v.caption,
         v.code
  from $coreScheme.v_user v
");

$stmt->execute();

while ( $row = $stmt->fetch() ) {
  $data[] = [
    'id' => $row['id'],
    'caption' => $row['caption'],
    'code' => $row['code']
  ];
}

if ($data) {
  echo json_encode($data);
} else {
  echo json_encode([]);
}

?>