<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");

require_once ('../db.php');

$stmt = $connect->prepare("
  select v.id,
         v.date_of::date,
         v.content
  from $db.v_affiche v
  order by v.date_of desc
  limit 3
");

$stmt->execute();

while ( $row = $stmt->fetch() ) {
  $data[] = [
    'id' => $row['id'],
    'date_of' => $row['date_of'],
    'content' => $row['content']
    ];
}

if ($data) {
  echo json_encode($data);
} else {
  echo json_encode([]);
}

?>