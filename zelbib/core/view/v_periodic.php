<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");

require_once ('../db.php');

$stmt = $connect->prepare("
  select v.id,
         v.caption,
         v.period,
         v.periodic_type,
         v.language,
         v.periodic_type_caption
  from $db.v_periodic v
  order by v.caption
");

$stmt->execute();

while ( $row = $stmt->fetch() ) {
  $data[] = [
    'id' => $row['id'],
    'caption' => $row['caption'],
    'period' => $row['period'],
    'periodic_type' => $row['periodic_type'],
    'language' => $row['language'],
    'periodic_type_caption' => $row['periodic_type_caption']
  ];
}

if ($data) {
  echo json_encode($data);
} else {
  echo json_encode([]);
}

?>