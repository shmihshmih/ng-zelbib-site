<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");

require_once ('../db.php');

$stmt = $connect->prepare("
  select v.id,
         v.date_of,
         v.title
  from $db.v_today v
  where extract(day from v.date_of) = extract(day from current_date)
  and extract(month from v.date_of) = extract(month from current_date)
");

$stmt->execute();

while ( $row = $stmt->fetch() ) {
  $data[] = [
    'id' => $row['id'],
    'date_of' => $row['date_of'],
    'title' => $row['title']
  ];
}

if ($data) {
  echo json_encode($data);
} else {
  echo json_encode([]);
}

?>