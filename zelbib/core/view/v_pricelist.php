<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");

require_once ('../db.php');

$stmt = $connect->prepare("
  select v.id,
         v.caption,
         v.pid,
         v.cost,
         v.per_one
  from $db.v_pricelist v
");

$stmt->execute();

while ( $row = $stmt->fetch() ) {
  $data[] = [
    'id' => $row['id'],
    'caption' => $row['caption'],
    'pid' => $row['pid'],
    'cost' => $row['cost'],
    'per_one' => $row['per_one']
  ];
}

if ($data) {
  echo json_encode($data);
} else {
  echo json_encode([]);
}

?>