<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");

require_once ('../db.php');

$stmt = $connect->prepare("
  select v.id,
         v.adress,
         v.phone,
         v.email,
         v.site,
         v.telegram,
         v.watsapp,
         v.vk
  from $db.v_adress v
");

$stmt->execute();

while ( $row = $stmt->fetch() ) {
  $data[] = [
    'id' => $row['id'],
    'adress' => $row['adress'],
    'phone' => $row['phone'],
    'email' => $row['email'],
    'site' => $row['site'],
    'telegram' => $row['telegram'],
    'watsapp' => $row['watsapp'],
    'vk' => $row['vk']
  ];
}

if ($data) {
  echo json_encode($data);
} else {
  echo json_encode([]);
}

?>