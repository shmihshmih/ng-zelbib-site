<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ('../db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);

if(isset($decodedData['flashbook'])) {
  $stmt = $connect->prepare("SELECT $db.f_flashbook8mod(
                                                   :id,
                                                   :caption,
                                                   :link,
                                                   :img)");

  $stmt->execute(array(
      'id'       => $decodedData['flashbook']['id'],
      'caption'  => $decodedData['flashbook']['caption'],
      'link'     => $decodedData['flashbook']['link'],
      'img'      => $decodedData['flashbook']['img']
    )
  );

  $err = $stmt->fetch();

  if($err) {
    echo $err[2];
  }
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_flashbook8del(?)");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();

  $arr = $stmt->errorInfo();

  if($arr) {
    echo $arr;
  }
}
?>