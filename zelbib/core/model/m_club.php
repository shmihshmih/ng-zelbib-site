<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ('../db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);

if(isset($decodedData['club'])) {
  $stmt = $connect->prepare("SELECT $db.f_club8mod(
                                                   :id,
                                                   :caption,
                                                   :description,
                                                   :dates,
                                                   :age)");

  $stmt->execute(array(
      'id'             => $decodedData['club']['id'],
      'caption'        => $decodedData['club']['caption'],
      'description'    => $decodedData['club']['description'],
      'dates'          => $decodedData['club']['dates'],
      'age'            => $decodedData['club']['age']
    )
  );

  $err = $stmt->fetch();

  if($err) {
    echo $err[2];
  }
}
//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_club8del(?)");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();

  $arr = $stmt->errorInfo();

  if($arr) {
    echo $arr;
  }
}
?>