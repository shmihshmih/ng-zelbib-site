<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ('../db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);

if(isset($decodedData['about_city'])) {
  $stmt = $connect->prepare("SELECT $db.f_about_city8mod(
                                                   :id,
                                                   :caption,
                                                   :when)");

  $stmt->execute(array(
      'id'       => $decodedData['about_city']['id'],
      'caption'  => $decodedData['about_city']['caption'],
      'when'     => $decodedData['about_city']['when']
    )
  );

  $err = $stmt->fetch();

  if($err) {
    echo $err[2];
  }

}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_about_city8del(?)");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();

  $arr = $stmt->errorInfo();

  if($arr) {
    echo $arr;
  }
}
?>