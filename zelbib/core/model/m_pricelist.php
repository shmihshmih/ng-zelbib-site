<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ('../db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);

if(isset($decodedData['pricelist'])) {
  $stmt = $connect->prepare("SELECT $db.f_pricelist8mod(
                                                   :id,
                                                   :caption,
                                                   :pid,
                                                   :cost,
                                                   :per_one)");

  $stmt->execute(array(
      'id'       => $decodedData['pricelist']['id'],
      'caption'  => $decodedData['pricelist']['caption'],
      'pid'      => $decodedData['pricelist']['pid'],
      'cost'     => $decodedData['pricelist']['cost'],
      'per_one'  => $decodedData['pricelist']['per_one']
    )
  );

  $err = $stmt->fetch();

  if($err) {
    echo $err[2];
  }
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_pricelist8del(?)");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();

  $arr = $stmt->errorInfo();

  if($arr) {
    echo $arr;
  }
}
?>