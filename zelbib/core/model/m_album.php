<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ('../db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);

if(isset($decodedData['album'])) {
  $stmt = $connect->prepare("SELECT $db.f_album8mod(
                                                   :id,
                                                   :date_of,
                                                   :title,
                                                   :cover)");

  $stmt->execute(array(
      'id'       => $decodedData['album']['id'],
      'date_of'  => $decodedData['album']['date_of'],
      'title'    => $decodedData['album']['title'],
      'cover'    => $decodedData['album']['cover']
    )
  );

  $err = $stmt->fetch();

  if($err) {
    echo $err[2];
  }
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_album8del(?)");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();

  $arr = $stmt->errorInfo();

  if($arr) {
    echo $arr;
  }
}
?>