<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ('../db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);

if(isset($decodedData['mode'])) {
  $stmt = $connect->prepare("SELECT $db.f_mode8mod(
                                                   :id,
                                                   :holiday,
                                                   :working,
                                                   :lunch,
                                                   :last_day,
                                                   :season)");

  $stmt->execute(array(
      'id'       => $decodedData['mode']['id'],
      'holiday'  => $decodedData['mode']['holiday'],
      'working'  => $decodedData['mode']['working'],
      'lunch'    => $decodedData['mode']['lunch'],
      'last_day' => $decodedData['mode']['last_day'],
      'season'   => $decodedData['mode']['season'],
    )
  );

  $err = $stmt->fetch();

  if($err) {
    echo $err[2];
  }
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_mode8del(?)");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();

  $arr = $stmt->errorInfo();

  if($arr) {
    echo $arr;
  }
}
?>