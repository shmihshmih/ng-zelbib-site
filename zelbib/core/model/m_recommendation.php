<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ('../db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);

if(isset($decodedData['recommendation'])) {
  $stmt = $connect->prepare("SELECT $db.f_recommendation8mod(
                                                   :id,
                                                   :rubric,
                                                   :caption,
                                                   :description,
                                                   :link)");

  $stmt->execute(array(
      'id'          => $decodedData['recommendation']['id'],
      'rubric'      => $decodedData['recommendation']['rubric'],
      'caption'     => $decodedData['recommendation']['caption'],
      'description' => $decodedData['recommendation']['description'],
      'link'        => $decodedData['recommendation']['link']
    )
  );

  $err = $stmt->fetch();

  if($err) {
    echo $err[2];
  }
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_recommendation8del(?)");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();

  $arr = $stmt->errorInfo();

  if($arr) {
    echo $arr;
  }
}
?>