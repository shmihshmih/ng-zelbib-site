<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ('../db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);

if(isset($decodedData['competition'])) {
  $stmt = $connect->prepare("SELECT $db.f_competition8mod(
                                                   :id,
                                                   :caption,
                                                   :description,
                                                   :dates,
                                                   :age)");

  $stmt->execute(array(
      'id'             => $decodedData['competition']['id'],
      'caption'        => $decodedData['competition']['caption'],
      'description'    => $decodedData['competition']['description'],
      'dates'          => $decodedData['competition']['dates'],
      'age'            => $decodedData['competition']['age']
    )
  );

  $err = $stmt->fetch();

  if($err) {
    echo $err[2];
  }
}


//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_competition8del(?)");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();

  $arr = $stmt->errorInfo();

  if($arr) {
    echo $arr;
  }
}
?>