<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ('../db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);

if(isset($decodedData['periodic'])) {
  $stmt = $connect->prepare("SELECT $db.f_periodic8mod(
                                                   :id,
                                                   :caption,
                                                   :period,
                                                   :periodic_type,
                                                   :language)");

  $stmt->execute(array(
      'id'               => $decodedData['periodic']['id'],
      'caption'          => $decodedData['periodic']['caption'],
      'period'           => $decodedData['periodic']['period'],
      'periodic_type'    => $decodedData['periodic']['periodic_type'],
      'language'         => $decodedData['periodic']['language'],
    )
  );

  $err = $stmt->fetch();

  if($err) {
    echo $err[2];
  }
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_periodic8del(?)");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();

  $arr = $stmt->errorInfo();

  if($arr) {
    echo $arr;
  }
}
?>