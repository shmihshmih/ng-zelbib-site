<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ('../db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);

if(isset($decodedData['adress'])) {
  $stmt = $connect->prepare("SELECT $db.f_adress8mod(
                                                   :id,
                                                   :adress,
                                                   :phone,
                                                   :email,
                                                   :site,
                                                   :telegram,
                                                   :watsapp,
                                                   :vk)");

  $stmt->execute(array(
      'id'       => $decodedData['adress']['id'],
      'adress'   => $decodedData['adress']['adress'],
      'phone'    => $decodedData['adress']['phone'],
      'email'    => $decodedData['adress']['email'],
      'site'     => $decodedData['adress']['site'],
      'telegram' => $decodedData['adress']['telegram'],
      'watsapp'  => $decodedData['adress']['watsapp'],
      'vk'       => $decodedData['adress']['vk']
    )
  );

  $err = $stmt->fetch();

  if($err) {
    echo $err[2];
  }
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_adress8del(?)");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();

  $arr = $stmt->errorInfo();

  if($arr) {
    echo $arr;
  }
}
?>