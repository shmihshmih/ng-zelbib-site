<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ('../db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);

if(isset($decodedData['photo'])) {
  $stmt = $connect->prepare("SELECT $db.f_photo8mod(
                                                   :id,
                                                   :caption,
                                                   :link,
                                                   :pid)");

  $stmt->execute(array(
      'id'       => $decodedData['photo']['id'],
      'caption'  => $decodedData['photo']['caption'],
      'link'     => $decodedData['photo']['link'],
      'pid'      => $decodedData['photo']['pid']
    )
  );

  $err = $stmt->fetch();

  if($err) {
    echo $err[2];
  }
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_photo8del(?)");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();

  $arr = $stmt->errorInfo();

  if($arr) {
    echo $arr;
  }
}
?>