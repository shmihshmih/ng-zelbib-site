<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ('../db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);

if(isset($decodedData['article'])) {
  $stmt = $connect->prepare("SELECT $db.f_article8mod(
                                                   :id,
                                                   :date_of,
                                                   :title,
                                                   :preview_annotation,
                                                   :content,
                                                   :preview_image,
                                                   :content_image,
                                                   :author)");

  $stmt->execute(array(
      'id'                    => $decodedData['article']['id'],
      'date_of'               => $decodedData['article']['date_of'],
      'title'                 => $decodedData['article']['title'],
      'preview_annotation'    => $decodedData['article']['preview_annotation'],
      'content'               => $decodedData['article']['content'],
      'preview_image'         => $decodedData['article']['preview_image'],
      'content_image'         => $decodedData['article']['content_image'],
      'author'                => $decodedData['article']['author'],
    )
  );

  $err = $stmt->fetch();

  if($err) {
    echo $err[2];
  }
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_article8del(?)");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();

  $arr = $stmt->errorInfo();

  if($arr) {
    echo $arr;
  }
}
?>