import { ZelbibPage } from './app.po';

describe('zelbib App', () => {
  let page: ZelbibPage;

  beforeEach(() => {
    page = new ZelbibPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
