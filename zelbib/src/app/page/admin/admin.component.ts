import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Form} from "@angular/forms";

import { MainService } from "../../service/main/main.service";
import { AdminService } from "../../service/admin/admin.service";

import { Today } from "../../service/model/today.model";
import {Article} from "../../service/model/article.model";
import {Periodic} from "../../service/model/periodic.model";
import {PeriodicType} from "../../service/model/periodic_type.model";
import {Club} from "../../service/model/club.model";
import {Pricelist} from "../../service/model/pricelist.model";
import {Competition} from "../../service/model/competition.model";
import {Recommendation} from "../../service/model/recommendation.model";
import {Affiche} from "../../service/model/affiche.model";
import {AboutCity} from "../../service/model/about_city.model";
import {Album} from "../../service/model/album.model";
import {Flashbook} from "../../service/model/flashbook.model";
import {Material} from "../../service/model/material.model";
import {myDocument} from "../../service/model/document.model";
import {Mode} from "app/service/model/mode.model";
import {Adress} from "../../service/model/adress.model";
import {Photo} from "../../service/model/photo.model";
import {GuestBook} from "../../service/model/guest_book.model";


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  providers: [MainService, AdminService]
})
export class AdminComponent implements OnInit, OnDestroy {

  visibility = {
    todayL: 'active',
    todayE: 'inactive',
    articleL: 'active',
    articleE: 'inactive',
    periodicL: 'active',
    periodicE: 'inactive',
    clubL: 'active',
    clubE: 'inactive',
    pricelistL: 'active',
    pricelistE: 'inactive',
    competitionL: 'active',
    competitionE: 'inactive',
    recommendationL: 'active',
    recommendationE: 'inactive',
    afficheL: 'active',
    afficheE: 'inactive',
    about_cityL: 'active',
    about_cityE: 'inactive',
    albumL: 'active',
    albumE: 'inactive',
    flashbookL: 'active',
    flashbookE: 'inactive',
    documentL: 'active',
    documentE: 'inactive',
    materialL: 'active',
    materialE: 'inactive',
    photoL: 'active',
    photoE: 'inactive'
  };

  todays: Today[] = [];
  articles: Article[] = [];
  users: any[] = [];
  periodics: Periodic[] = [];
  periodic_types: PeriodicType[] = [];
  clubs: Club[] = [];
  pricelists: Pricelist[] = [];
  competitions: Competition[] = [];
  recommendations: Recommendation[] = [];
  affiches: Affiche[] = [];
  about_cities: AboutCity[] = [];
  albums: Album[] = [];
  flashbooks: Flashbook[] = [];
  documents: myDocument[] = [];
  materials: Material[] = [];
  mode: Mode[] = [];
  adress: Adress = new Adress;
  photos: Photo[] = [];
  guest_book: GuestBook[] = [];

  todayEditForm: FormGroup;
  articleEditForm: FormGroup;
  periodicEditForm: FormGroup;
  clubEditForm: FormGroup;
  pricelistEditForm: FormGroup;
  competitionEditForm: FormGroup;
  recommendationEditForm: FormGroup;
  afficheEditForm: FormGroup;
  about_cityEditForm: FormGroup;
  albumEditForm: FormGroup;
  flashbookEditForm: FormGroup;
  documentEditForm: FormGroup;
  materialEditForm: FormGroup;
  summerModeEditForm: FormGroup;
  winterModeEditForm: FormGroup;
  adressEditForm: FormGroup;
  photoEditForm: FormGroup;

  activeToday: Today = new Today();
  activeArticle: Article = new Article();
  activePeriodic: Periodic = new Periodic();
  activeClub: Club = new Club();
  activePricelist: Pricelist = new Pricelist();
  activeCompetition: Competition = new Competition();
  activeRecommendation: Recommendation = new Recommendation();
  activeAffiche: Affiche = new Affiche();
  activeAbout_city: AboutCity = new AboutCity();
  activeAlbum: Album = new Album();
  activeFlashbook: Flashbook = new Flashbook();
  activeDocument: myDocument = new myDocument();
  activeMaterial: Material = new Material();
  activePhoto: Photo = new Photo();

  toggleForm(formName: string): void {
    if (this.visibility[formName+'L'] == 'active') { this.visibility[formName+'L'] = 'inactive';} else { this.visibility[formName+'L'] = 'active' }
    if (this.visibility[formName+'E'] == 'active') { this.visibility[formName+'E'] = 'inactive';} else { this.visibility[formName+'E'] = 'active' }
  }

  constructor(
    public formBuilder: FormBuilder,
    private mainService: MainService,
    private adminService: AdminService
  ) {
    this.getTodaysList();
    this.getArticles();
    this.getUsers();
    this.getPeriodic_types();
    this.getPeriodics();
    this.getClubs();
    this.getPricelists();
    this.getCompetitions();
    this.getRecommendations();
    this.getAffiches();
    this.getAbout_cities();
    this.getAlbums();
    this.getFlashbooks();
    this.getDocuments();
    this.getMaterials();
    this.getMode();
    this.getAdress();
    this.getPhotos_for_admin();
    this.getGuest_book();
  }


  ngOnInit() {
    this.createForms();
  }

  ngOnDestroy() {
    localStorage.setItem('youCan', 'false');
  }

  getAdress() {
    this.mainService.getAdresses('v_adress')
      .subscribe(
        (adress) => {
          this.adress = adress[0] as Adress;
          this.adressEditForm = this.formBuilder.group({
            'id'       : new FormControl(this.adress.id),
            'adress'   : new FormControl(this.adress.adress),
            'phone'    : new FormControl(this.adress.phone),
            'email'    : new FormControl(this.adress.email),
            'site'     : new FormControl(this.adress.site),
            'telegram' : new FormControl(this.adress.telegram),
            'watsapp'  : new FormControl(this.adress.watsapp),
            'vk'       : new FormControl(this.adress.vk)
          });
          },
        (err) => {console.log(err)}
      )
  }

  getMode() {
    this.mainService.getModes('v_mode')
      .subscribe(
        (mode) => {
          this.mode = mode;
          this.summerModeEditForm = this.formBuilder.group({
            'id':       new FormControl(this.summerTime('id')),
            'holiday':  new FormControl(this.summerTime('holiday')),
            'working':  new FormControl(this.summerTime('working')),
            'lunch':    new FormControl(this.summerTime('lunch')),
            'last_day': new FormControl(this.summerTime('last_day')),
            'season':   new FormControl(this.summerTime('season'))
          });
          this.winterModeEditForm = this.formBuilder.group({
            'id':       new FormControl(this.winterTime('id')),
            'holiday':  new FormControl(this.winterTime('holiday')),
            'working':  new FormControl(this.winterTime('working')),
            'lunch':    new FormControl(this.winterTime('lunch')),
            'last_day': new FormControl(this.winterTime('last_day')),
            'season':   new FormControl(this.winterTime('season'))
          });
          },
        (err) => {console.log(err)}
      )
  }

  summerTime(key: string): any {
    let singleObj = this.mode.filter(mod => mod.season === 'summer')[0];
    if (singleObj) {
      return singleObj[key];
    } else {
      return false;
    }
  }
  winterTime(key: string): any {
    let singleObj = this.mode.filter(mod => mod.season === 'winter')[0];
    if (singleObj) {
      return singleObj[key];
    } else {
      return false;
    }
  }

  createForms(): void {
    this.todayEditForm = this.formBuilder.group({
      'id':       new FormControl(this.activeToday.id),
      'date_of' : new FormControl(this.activeToday.date_of),
      'title':    new FormControl(this.activeToday.title)
    });
    this.articleEditForm = this.formBuilder.group({
      'id':                   new FormControl(this.activeArticle.id),
      'date_of' :             new FormControl(this.activeArticle.date_of),
      'title':                new FormControl(this.activeArticle.title),
      'preview_annotation':   new FormControl(this.activeArticle.preview_annotation),
      'content':              new FormControl(this.activeArticle.content),
      'preview_image':        new FormControl(this.activeArticle.preview_image),
      'content_image':        new FormControl(this.activeArticle.content_image),
      'author':               new FormControl(this.activeArticle.author)
    });
    this.periodicEditForm = this.formBuilder.group({
      'id':            new FormControl(this.activePeriodic.id),
      'caption':       new FormControl(this.activePeriodic.caption),
      'period':        new FormControl(this.activePeriodic.period),
      'periodic_type': new FormControl(this.activePeriodic.periodic_type),
      'language':      new FormControl(this.activePeriodic.language)
    });
    this.clubEditForm = this.formBuilder.group({
      'id':            new FormControl(this.activeClub.id),
      'caption':       new FormControl(this.activeClub.caption),
      'description':   new FormControl(this.activeClub.description),
      'dates':         new FormControl(this.activeClub.dates),
      'age':           new FormControl(this.activeClub.age)
    });
    this.pricelistEditForm = this.formBuilder.group({
      'id':       new FormControl(this.activePricelist.id),
      'caption':  new FormControl(this.activePricelist.caption),
      'pid':      new FormControl(this.activePricelist.pid),
      'cost':     new FormControl(this.activePricelist.cost),
      'per_one':  new FormControl(this.activePricelist.per_one)
    });
    this.competitionEditForm = this.formBuilder.group({
      'id':            new FormControl(this.activeCompetition.id),
      'caption':       new FormControl(this.activeCompetition.caption),
      'description':   new FormControl(this.activeCompetition.description),
      'dates':         new FormControl(this.activeCompetition.dates),
      'age':           new FormControl(this.activeCompetition.age)
    });
    this.recommendationEditForm = this.formBuilder.group({
      'id':          new FormControl(this.activeRecommendation.id),
      'rubric':      new FormControl(this.activeRecommendation.rubric),
      'caption':     new FormControl(this.activeRecommendation.caption),
      'description': new FormControl(this.activeRecommendation.description),
      'link':        new FormControl(this.activeRecommendation.link),
    });
    this.afficheEditForm = this.formBuilder.group({
      'id':          new FormControl(this.activeAffiche.id),
      'date_of':     new FormControl(this.activeAffiche.date_of),
      'content':     new FormControl(this.activeAffiche.content)
    });
    this.about_cityEditForm = this.formBuilder.group({
      'id':       new FormControl(this.activeAbout_city.id),
      'caption':  new FormControl(this.activeAbout_city.caption),
      'when':     new FormControl(this.activeAbout_city.when)
    });
    this.albumEditForm = this.formBuilder.group({
      'id':       new FormControl(this.activeAlbum.id),
      'date_of':  new FormControl(this.activeAlbum.date_of),
      'title':    new FormControl(this.activeAlbum.title),
      'cover': new FormControl(this.activeAlbum.cover)
    });
    this.flashbookEditForm = this.formBuilder.group({
      'id':       new FormControl(this.activeFlashbook.id),
      'caption':  new FormControl(this.activeFlashbook.caption),
      'link':     new FormControl(this.activeFlashbook.link),
      'img':      new FormControl(this.activeFlashbook.img)
    });
    this.documentEditForm = this.formBuilder.group({
      'id':       new FormControl(this.activeDocument.id),
      'caption':  new FormControl(this.activeDocument.caption),
      'link':     new FormControl(this.activeDocument.link)
    });
    this.materialEditForm = this.formBuilder.group({
      'id':       new FormControl(this.activeMaterial.id),
      'caption':  new FormControl(this.activeMaterial.caption),
      'quantity': new FormControl(this.activeMaterial.quantity)
    });
    this.summerModeEditForm = this.formBuilder.group({
      'id':       new FormControl(this.summerTime('id')      ),
      'holiday':  new FormControl(this.summerTime('holiday') ),
      'working':  new FormControl(this.summerTime('working') ),
      'lunch':    new FormControl(this.summerTime('lunch')   ),
      'last_day': new FormControl(this.summerTime('last_day')),
      'season':   new FormControl(this.summerTime('season'))
    });
    this.winterModeEditForm = this.formBuilder.group({
      'id':       new FormControl(this.winterTime('id')      ),
      'holiday':  new FormControl(this.winterTime('holiday') ),
      'working':  new FormControl(this.winterTime('working') ),
      'lunch':    new FormControl(this.winterTime('lunch')   ),
      'last_day': new FormControl(this.winterTime('last_day')),
      'season':   new FormControl(this.winterTime('season'))
    });
    this.adressEditForm = this.formBuilder.group({
      'id'       : new FormControl(this.adress.id),
      'adress'   : new FormControl(this.adress.adress),
      'phone'    : new FormControl(this.adress.phone),
      'email'    : new FormControl(this.adress.email),
      'site'     : new FormControl(this.adress.site),
      'telegram' : new FormControl(this.adress.telegram),
      'watsapp'  : new FormControl(this.adress.watsapp),
      'vk'       : new FormControl(this.adress.vk)
    });
    this.photoEditForm = this.formBuilder.group({
      'id'       : new FormControl(this.activePhoto.id),
      'caption'  : new FormControl(this.activePhoto.caption),
      'link'     : new FormControl(this.activePhoto.link),
      'pid'      : new FormControl(this.activePhoto.pid)
    });
  }

  //lists
  getTodaysList() {
    this.mainService.getTodaysList('v_today_list')
      .subscribe((todays) => {this.todays = todays || []})
  }

  getArticles() {
    this.mainService.getArticles('v_article')
      .subscribe((articles) => {this.articles = articles || []})
  }

  getUsers() {
    this.adminService.getUsers('v_user')
      .subscribe((users) => {this.users = users || []})
  }

  getPeriodics() {
    this.mainService.getPeriodics('v_periodic')
      .subscribe((periodics) => {this.periodics = periodics || []})
  }

  getPeriodic_types() {
    this.mainService.getPeriodicTypes('v_periodic_type')
      .subscribe((periodic_types) => {this.periodic_types = periodic_types || []})
  }

  getClubs() {
    this.mainService.getClubs('v_club')
      .subscribe((clubs) => {this.clubs = clubs || []})
  }

  getPricelists() {
    this.mainService.getPricelists('v_pricelist')
      .subscribe((pricelists) => {this.pricelists = pricelists || []})
  }

  getCompetitions() {
    this.mainService.getCompetitions('v_competition')
      .subscribe((competitions) => {this.competitions = competitions || []})
  }

  getRecommendations() {
    this.mainService.getRecommendations('v_recommendation')
      .subscribe((recommendations) => {this.recommendations = recommendations || []})
  }

  getAffiches() {
    this.mainService.getAffiches('v_affiche')
      .subscribe((affiches) => {this.affiches = affiches || []})
  }

  getAbout_cities() {
    this.mainService.getAboutCities('v_about_city')
      .subscribe((about_cities) => {this.about_cities = about_cities || []})
  }

  getAlbums() {
    this.mainService.getAlbums('v_album')
      .subscribe((albums) => {this.albums = albums || []})
  }

  getFlashbooks() {
    this.mainService.getFlashbooks('v_flashbook')
      .subscribe((flashbooks) => {this.flashbooks = flashbooks || []})
  }

  getDocuments() {
    this.mainService.getDocuments('v_document')
      .subscribe((documents) => {this.documents = documents || []})
  }

  getMaterials() {
    this.mainService.getMaterials('v_material')
      .subscribe((materials) => {this.materials = materials || []})
  }

  getGuest_book() {
    this.mainService.getGuest_book('v_guest_book')
      .subscribe((guest_book) => {this.guest_book = guest_book || []})
  }

  //getPhotos(viewName: string, albumId: number) {
  //  this.mainService.getPhotos(viewName, albumId)
  //    .subscribe(
  //      (photos) => this.photos = photos
  //    );
  //}
  getPhotos_for_admin() {
    this.mainService.getPhotos_for_admin('v_photos_for_admin')
      .subscribe((photos) => {this.photos = photos || []})
  }

  //get
  getToday(id: number) {
    this.mainService.getTodaysList('v_today_list')
      .subscribe(
        (todays) => {
          this.activeToday = todays.find(today => today.id === id);
          this.todayEditForm.patchValue({
            'id':       this.activeToday.id,
            'date_of':  this.activeToday.date_of,
            'title':    this.activeToday.title
          });
          this.toggleForm('today');
        }
      )
  }

  getArticle(id: number) {
    this.mainService.getArticles('v_article')
      .subscribe(
        (articles) => {
          this.activeArticle = articles.find(article => article.id === id);
          this.articleEditForm.patchValue({
            'id':                 this.activeArticle.id,
            'date_of' :           this.activeArticle.date_of,
            'title':              this.activeArticle.title,
            'preview_annotation': this.activeArticle.preview_annotation,
            'content':            this.activeArticle.content,
            'preview_image':      this.activeArticle.preview_image,
            'content_image':      this.activeArticle.content_image,
            'author':             this.activeArticle.author
          });
          this.toggleForm('article');
        }
      )
  }

  getPeriodic(id: number) {
    this.mainService.getPeriodics('v_periodic')
      .subscribe(
        (periodics) => {
          this.activePeriodic = periodics.find(periodic => periodic.id === id);
          this.periodicEditForm.patchValue({
            'id':            this.activePeriodic.id,
            'caption' :      this.activePeriodic.caption,
            'period':        this.activePeriodic.period,
            'periodic_type': this.activePeriodic.periodic_type,
            'language':      this.activePeriodic.language
          });
          this.toggleForm('periodic');
        }
      )
  }

  getClub(id: number) {
    this.mainService.getClubs('v_club')
      .subscribe(
        (clubs) => {
          this.activeClub = clubs.find(club => club.id === id);
          this.clubEditForm.patchValue({
            'id':            this.activeClub.id,
            'caption' :      this.activeClub.caption,
            'description':   this.activeClub.description,
            'dates':         this.activeClub.dates,
            'age':           this.activeClub.age
          });
          this.toggleForm('club');
        }
      )
  }

  getPricelist(id: number) {
    this.mainService.getPricelists('v_pricelist')
      .subscribe(
        (pricelists) => {
          this.activePricelist = pricelists.find(pricelist => pricelist.id === id);
          this.pricelistEditForm.patchValue({
            'id':            this.activePricelist.id,
            'caption' :      this.activePricelist.caption,
            'pid':           this.activePricelist.pid,
            'cost':          this.activePricelist.cost,
            'per_one':       this.activePricelist.per_one
          });
          this.toggleForm('pricelist');
        }
      )
  }

  getCompetition(id: number) {
    this.mainService.getCompetitions('v_competition')
      .subscribe(
        (competitions) => {
          this.activeCompetition = competitions.find(competition => competition.id === id);
          this.competitionEditForm.patchValue({
            'id':            this.activeCompetition.id,
            'caption' :      this.activeCompetition.caption,
            'description':   this.activeCompetition.description,
            'dates':         this.activeCompetition.dates,
            'age':           this.activeCompetition.age
          });
          this.toggleForm('competition');
        }
      )
  }

  getRecommendation(id: number) {
    this.mainService.getRecommendations('v_recommendation')
      .subscribe(
        (recommendations) => {
          this.activeRecommendation = recommendations.find(recommendation => recommendation.id === id);
          this.recommendationEditForm.patchValue({
            'id':           this.activeRecommendation.id,
            'rubric' :      this.activeRecommendation.rubric,
            'caption':      this.activeRecommendation.caption,
            'description':  this.activeRecommendation.description,
            'link':         this.activeRecommendation.link
          });
          this.toggleForm('recommendation');
        }
      )
  }

  getAffiche(id: number) {
    this.mainService.getAffiches('v_affiche')
      .subscribe(
        (affiches) => {
          this.activeAffiche = affiches.find(affiche => affiche.id === id);
          this.afficheEditForm.patchValue({
            'id':           this.activeAffiche.id,
            'date_of' :     this.activeAffiche.date_of,
            'content':      this.activeAffiche.content
          });
          this.toggleForm('affiche');
        }
      )
  }

  getAbout_city(id: number) {
    this.mainService.getAboutCities('v_about_city')
      .subscribe(
        (about_cities) => {
          this.activeAbout_city = about_cities.find(about_city => about_city.id === id);
          this.about_cityEditForm.patchValue({
            'id':        this.activeAbout_city.id,
            'caption' :  this.activeAbout_city.caption,
            'when':      this.activeAbout_city.when
          });
          this.toggleForm('about_city');
        }
      )
  }

  getAlbum(id: number) {
    this.mainService.getAlbums('v_album')
      .subscribe(
        (albums) => {
          this.activeAlbum = albums.find(album => album.id === id) || new Album();
          this.albumEditForm.patchValue({
            'id':        this.activeAlbum.id,
            'date_of' :  this.activeAlbum.date_of,
            'title':     this.activeAlbum.title,
            'cover':     this.activeAlbum.cover
          });
          this.toggleForm('album');
          //this.getPhotos('v_photo', this.activeAlbum.id);
        }
      )
  }

  getFlashbook(id: number) {
    this.mainService.getFlashbooks('v_flashbook')
      .subscribe(
        (flashbooks) => {
          this.activeFlashbook = flashbooks.find(flashbook => flashbook.id === id);
          this.flashbookEditForm.patchValue({
            'id':        this.activeFlashbook.id,
            'caption' :  this.activeFlashbook.caption,
            'link':      this.activeFlashbook.link,
            'img':       this.activeFlashbook.img
          });
          this.toggleForm('flashbook');
        }
      )
  }

  getDocument(id: number) {
    this.mainService.getDocuments('v_document')
      .subscribe(
        (documents) => {
          this.activeDocument = documents.find(document => document.id === id);
          this.documentEditForm.patchValue({
            'id':        this.activeDocument.id,
            'caption' :  this.activeDocument.caption,
            'link':      this.activeDocument.link
          });
          this.toggleForm('document');
        }
      )
  }

  getMaterial(id: number) {
    this.mainService.getMaterials('v_material')
      .subscribe(
        (materials) => {
          this.activeMaterial = materials.find(material => material.id === id);
          this.materialEditForm.patchValue({
            'id':        this.activeMaterial.id,
            'caption' :  this.activeMaterial.caption,
            'quantity':  this.activeMaterial.quantity
          });
          this.toggleForm('material');
        }
      )
  }

  getPhoto(id: number) {
    this.mainService.getPhotos_for_admin('v_photos_for_admin')
      .subscribe(
        (photos) => {
          this.activePhoto = photos.find(photo => photo.id === id);
          this.photoEditForm.patchValue({
            'id':        this.activePhoto.id,
            'caption' :  this.activePhoto.caption,
            'link':  this.activePhoto.link,
            'pid':  this.activePhoto.pid
          });
          this.toggleForm('photo');
        }
      )
  }


  //del
  delToday(id: number) {
    this.adminService.delToday(id, 'today')
      .catch(err => {return err})
      .subscribe(
        () => {this.getTodaysList()},
        (err) => console.log(err)
      );
  }
  delArticle(id: number) {
    this.adminService.delArticle(id, 'article')
      .catch(err => {return err})
      .subscribe(
        () => {this.getArticles()},
        (err) => console.log(err)
      );
  }
  delPeriodic(id: number) {
    this.adminService.delPeriodic(id, 'periodic')
      .catch(err => {return err})
      .subscribe(
        () => {this.getPeriodics()},
        (err) => console.log(err)
      );
  }
  delClub(id: number) {
    this.adminService.delClub(id, 'club')
      .catch(err => {return err})
      .subscribe(
        () => {this.getClubs()},
        (err) => console.log(err)
      );
  }
  delPricelist(id: number) {
    this.adminService.delPricelist(id, 'pricelist')
      .catch(err => {return err})
      .subscribe(
        () => {this.getPricelists()},
        (err) => console.log(err)
      );
  }
  delCompetition(id: number) {
    this.adminService.delCompetition(id, 'competition')
      .catch(err => {return err})
      .subscribe(
        () => {this.getCompetitions()},
        (err) => console.log(err)
      );
  }
  delRecommendation(id: number) {
    this.adminService.delRecommendation(id, 'recommendation')
      .catch(err => {return err})
      .subscribe(
        () => {this.getRecommendations()},
        (err) => console.log(err)
      );
  }
  delAffiche(id: number) {
    this.adminService.delAffiche(id, 'affiche')
      .catch(err => {return err})
      .subscribe(
        () => {this.getAffiches()},
        (err) => console.log(err)
      );
  }
  delAbout_city(id: number) {
    this.adminService.delAbout_city(id, 'about_city')
      .catch(err => {return err})
      .subscribe(
        () => {this.getAbout_cities()},
        (err) => console.log(err)
      );
  }
  delAlbum(id: number) {
    this.adminService.delAlbum(id, 'album')
      .catch(err => {return err})
      .subscribe(
        () => {this.getAlbums()},
        (err) => console.log(err)
      );
  }
  delFlashbook(id: number) {
    this.adminService.delFlashbook(id, 'flashbook')
      .catch(err => {return err})
      .subscribe(
        () => {this.getFlashbooks()},
        (err) => console.log(err)
      );
  }
  delDocument(id: number) {
    this.adminService.delDocument(id, 'document')
      .catch(err => {return err})
      .subscribe(
        () => {this.getDocuments()},
        (err) => console.log(err)
      );
  }
  delMaterial(id: number) {
    this.adminService.delMaterial(id, 'material')
      .catch(err => {return err})
      .subscribe(
        () => {this.getMaterials()},
        (err) => console.log(err)
      );
  }

  delPhoto(id: number) {
    this.adminService.delPhoto(id, 'photo')
      .catch(err => {return err})
      .subscribe(
        () => {this.getPhotos_for_admin()},
        (err) => console.log(err)
      );
  }

  delGuest_book(id: number) {
    this.adminService.delGuest_book(id, 'guest_book')
      .catch(err => {return err})
      .subscribe(
        () => {this.getGuest_book()},
        (err) => console.log(err)
      );
  }


  //submit
  todaySubmit(): void {
    this.adminService.modToday(this.todayEditForm.value, 'today')
      .catch(err => {return err})
      .subscribe(
        () => {this.getTodaysList()},
        (err) => {console.log(err); }
        );
    this.visibility.todayE = 'inactive';
    this.visibility.todayL = 'active';
  }

  articleSubmit(): void {
    this.adminService.modArticle(this.articleEditForm.value, 'article')
      .catch(err => {return err})
      .subscribe(
        () => {this.getArticles()},
        (err) => {console.log(err); }
      );
    this.visibility.articleE = 'inactive';
    this.visibility.articleL = 'active';
  }

  periodicSubmit(): void {
    this.adminService.modPeriodic(this.periodicEditForm.value, 'periodic')
      .catch(err => {return err})
      .subscribe(
        () => {this.getPeriodics()},
        (err) => {console.log(err); }
      );
    this.visibility.periodicE = 'inactive';
    this.visibility.periodicL = 'active';
  }

  clubSubmit(): void {
    this.adminService.modClub(this.clubEditForm.value, 'club')
      .catch(err => {return err})
      .subscribe(
        () => {this.getClubs()},
        (err) => {console.log(err); }
      );
    this.visibility.clubE = 'inactive';
    this.visibility.clubL = 'active';
  }

  pricelistSubmit(): void {
    this.adminService.modPricelist(this.pricelistEditForm.value, 'pricelist')
      .catch(err => {return err})
      .subscribe(
        () => {this.getPricelists()},
        (err) => {console.log(err); }
      );
    this.visibility.pricelistE = 'inactive';
    this.visibility.pricelistL = 'active';
  }

  competitionSubmit(): void {
    this.adminService.modCompetition(this.competitionEditForm.value, 'competition')
      .catch(err => {return err})
      .subscribe(
        () => {this.getCompetitions()},
        (err) => {console.log(err); }
      );
    this.visibility.competitionE = 'inactive';
    this.visibility.competitionL = 'active';
  }

  recommendationSubmit(): void {
    this.adminService.modRecommendation(this.recommendationEditForm.value, 'recommendation')
      .catch(err => {return err})
      .subscribe(
        () => {this.getRecommendations()},
        (err) => {console.log(err); }
      );
    this.visibility.recommendationE = 'inactive';
    this.visibility.recommendationL = 'active';
  }

  afficheSubmit(): void {
    this.adminService.modAffiche(this.afficheEditForm.value, 'affiche')
      .catch(err => {return err})
      .subscribe(
        () => {this.getAffiches()},
        (err) => {console.log(err); }
      );
    this.visibility.afficheE = 'inactive';
    this.visibility.afficheL = 'active';
  }

  about_citySubmit(): void {
    this.adminService.modAbout_city(this.about_cityEditForm.value, 'about_city')
      .catch(err => {return err})
      .subscribe(
        () => {this.getAbout_cities()},
        (err) => {console.log(err); }
      );
    this.visibility.about_cityE = 'inactive';
    this.visibility.about_cityL = 'active';
  }
  albumSubmit(): void {
    this.adminService.modAlbum(this.albumEditForm.value, 'album')
      .catch(err => {return err})
      .subscribe(
        () => {this.getAlbums()},
        (err) => {console.log(err); }
      );
    this.visibility.albumE = 'inactive';
    this.visibility.albumL = 'active';
  }
  flashbookSubmit(): void {
    this.adminService.modFlashbook(this.flashbookEditForm.value, 'flashbook')
      .catch(err => {return err})
      .subscribe(
        () => {this.getFlashbooks()},
        (err) => {console.log(err); }
      );
    this.visibility.flashbookE = 'inactive';
    this.visibility.flashbookL = 'active';
  }
  documentSubmit(): void {
    this.adminService.modDocument(this.documentEditForm.value, 'document')
      .catch(err => {return err})
      .subscribe(
        () => {this.getDocuments()},
        (err) => {console.log(err); }
      );
    this.visibility.documentE = 'inactive';
    this.visibility.documentL = 'active';
  }
  materialSubmit(): void {
    this.adminService.modMaterial(this.materialEditForm.value, 'material')
      .catch(err => {return err})
      .subscribe(
        () => {this.getMaterials()},
        (err) => {console.log(err); }
      );
    this.visibility.materialE = 'inactive';
    this.visibility.materialL = 'active';
  }
  winterModeSubmit(): void {
    this.adminService.modMode(this.winterModeEditForm.value, 'mode')
      .catch(err => {return err})
      .subscribe(
        () => {this.getMode();},
        (err) => {console.log(err); }
      );
  }
  summerModeSubmit(): void {
    this.adminService.modMode(this.summerModeEditForm.value, 'mode')
      .catch(err => {return err})
      .subscribe(
        () => {this.getMode();},
        (err) => {console.log(err); }
      );
  }
  adressSubmit(): void {
    this.adminService.modAdress(this.adressEditForm.value, 'adress')
      .catch(err => {return err})
      .subscribe(
        () => {this.getAdress()},
        (err) => {console.log(err); }
      );
  }
  photoSubmit(): void {
    this.adminService.modPhoto(this.photoEditForm.value, 'photo')
      .catch(err => {return err})
      .subscribe(
        () => {this.getPhotos_for_admin()},
        (err) => {console.log(err); }
      );
    this.visibility.photoE = 'inactive';
    this.visibility.photoL = 'active';
  }

}
