import { Component, OnInit } from '@angular/core';
import {MainService} from "../../service/main/main.service";
import {Article} from "../../service/model/article.model";

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  articles: Article[] = [];
  constructor(
    private mainService: MainService
  ) {
    this.getArticles();
  }

  ngOnInit() {
  }

  getArticles() {
    this.mainService.getArticles('v_article')
      .subscribe(
        (articles) => {this.articles = articles || []}
      )
  }
}
