import {Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//import {MainService} from "../../service/main/main.service";

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss']
})
export class PhotoComponent implements OnInit {


  //photos: any[] = [];

  public metadataUri: string;
  public flexBorderSize: number = 3;
  public flexImageSize: number = 7;
  public galleryName: string = 'aas';

  constructor(
    public route: ActivatedRoute
    //private mainService: MainService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      (val) => {
        this.metadataUri = 'http://localhost/ng-zelbib-site/ng-zelbib-site/zelbib/core/view/v_photo.php?pid='+val.id;
      }
    );
  }

  //getPhoto(albumId:number): void {
  //  this.mainService.getPhoto('v_photo', albumId)
  //    .subscribe(
  //      (photos) => {this.photos = photos;console.log(photos);}
  //    )
  //}

  onViewerVisibilityChanged(isVisible: boolean) {
    console.log('viewer visible: ' + isVisible)
  }
}
