import { Component, OnInit } from '@angular/core';
import {MainService} from "../../service/main/main.service";
import {GuestBook} from "../../service/model/guest_book.model";
import {FormGroup, FormControl, FormBuilder, Form} from "@angular/forms";
import {AdminService} from "../../service/admin/admin.service";

@Component({
  selector: 'app-guest-book',
  templateUrl: './guest-book.component.html',
  styleUrls: ['./guest-book.component.scss']
})
export class GuestBookComponent implements OnInit {

  guest_book: GuestBook[] = [];
  guest_bookEditForm: FormGroup;

  captchaCorrect: boolean = false;

  constructor(
    private adminService: AdminService,
    private mainService: MainService,
    public formBuilder: FormBuilder
  ) {
    this.getGuestBook();
  }

  ngOnInit() {
    this.createForm();
  }

  getGuestBook() {
    this.mainService.getGuest_book('v_guest_book')
      .subscribe(
        (guest_book) => {this.guest_book = guest_book  || []}
      )
  }

  createForm():void {
    this.guest_bookEditForm = this.formBuilder.group({
      'author' : new FormControl(),
      'email'  : new FormControl(),
      'content': new FormControl()
    })
  }

  guest_bookSubmit() {
    if(this.captchaCorrect == true) {
      console.log('hello from component');
      console.log(this.guest_bookEditForm.value);
      this.adminService.modGuest_book(this.guest_bookEditForm.value, 'guest_book')
        .catch(err => {return err})
        .subscribe(
          () => {this.getGuestBook()},
          (err) => {console.log(err); }
        );
    } else {
      alert('Неверная капча!');
    }

  }

  resolved(captchaResponse: string) {
    this.captchaCorrect = true;
    console.log(`Resolved captcha with response ${captchaResponse}:`);
  }

}
