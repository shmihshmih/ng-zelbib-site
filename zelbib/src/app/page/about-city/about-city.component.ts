import { Component, OnInit } from '@angular/core';
import {MainService} from "../../service/main/main.service";
import {AboutCity} from "../../service/model/about_city.model";

@Component({
  selector: 'app-about-city',
  templateUrl: './about-city.component.html',
  styleUrls: ['./about-city.component.scss']
})
export class AboutCityComponent implements OnInit {
  about_city: AboutCity[];

  constructor(
    private mainService: MainService
  ) { }

  ngOnInit() {
    this.getAboutCities();
  }

  getAboutCities() {
    this.mainService.getAboutCities('v_about_city')
      .subscribe(
        (about_city) => {this.about_city = about_city || [];},
        (err) => {console.log(err)}
      )
  }

}