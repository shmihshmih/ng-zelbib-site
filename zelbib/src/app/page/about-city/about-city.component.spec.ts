import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutCityComponent } from './about-city.component';

describe('AboutCityComponent', () => {
  let component: AboutCityComponent;
  let fixture: ComponentFixture<AboutCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
