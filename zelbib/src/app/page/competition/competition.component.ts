import { Component, OnInit } from '@angular/core';
import {MainService} from "../../service/main/main.service";
import {Competition} from "../../service/model/competition.model";

@Component({
  selector: 'app-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.scss']
})
export class CompetitionComponent implements OnInit {
  competitions: Competition[] = [];
  constructor(
    private mainService: MainService
  ) {
    this.getCompetitions();
  }

  ngOnInit() {
  }

  getCompetitions() {
    this.mainService.getCompetitions('v_competition')
      .subscribe(
        (competitions) => {this.competitions = competitions || []}
      )
  }
}
