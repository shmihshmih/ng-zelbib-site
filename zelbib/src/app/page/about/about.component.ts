import { Component, OnInit } from '@angular/core';
import {MainService} from "../../service/main/main.service";
import {Periodic} from "../../service/model/periodic.model";
import {PeriodicType} from "../../service/model/periodic_type.model";
import {Mode} from "../../service/model/mode.model";
import {Adress} from "../../service/model/adress.model";
import {Material} from "../../service/model/material.model";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  periodics: Periodic[] = [];
  periodic_types: PeriodicType[] = [];
  mode: Mode[] = [];
  adress: Adress ;
  base: Material[] = [];

  summerTime(key: string): any {
    let singleObj = this.mode.filter(mod => mod.season === 'summer')[0];
    if (singleObj) {
      return singleObj[key];
    } else {
      return false;
    }
  }
  winterTime(key: string): any {
    let singleObj = this.mode.filter(mod => mod.season === 'winter')[0];
    if (singleObj) {
      return singleObj[key];
    } else {
      return false;
    }
  }
  constructor(
    private mainService: MainService
  ) {
    this.getMode();
    this.getAdress();
    this.getPeriodics();
    this.getPeriodicTypes();
    this.getAdress();
    this.getMaterials();
  }

  ngOnInit() {

  }

  getPeriodics() {
    this.mainService.getPeriodics('v_periodic')
      .subscribe(
        (periodics) => {this.periodics = periodics || [];},
        (err) => {console.log(err)}
      )
  }
  getPeriodicTypes() {
    this.mainService.getPeriodicTypes('v_periodic_type')
      .subscribe(
        (periodic_types) => {this.periodic_types = periodic_types || [];},
        (err) => {console.log(err)}
      )
  }
  getAdress() {
    this.mainService.getAdresses('v_adress')
      .subscribe(
        (adress) => {this.adress = adress[0] as Adress;},
        (err) => {console.log(err)}
      )
  }
  getMaterials() {
    this.mainService.getMaterials('v_material')
      .subscribe(
        (base) => {this.base = base || [];},
        (err) => {console.log(err)}
      )
  }
  getMode() {
    this.mainService.getModes('v_mode')
      .subscribe(
        (mode) => {this.mode = mode;},
        (err) => {console.log(err)}
      )
  }

}
