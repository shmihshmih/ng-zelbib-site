import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {LoginService} from "../../service/login/login.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  constructor(
    public fb: FormBuilder,
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit() {
    this.createForm();
  }

  loginEditForm: FormGroup;

  createForm(): void {
    this.loginEditForm = this.fb.group({
      'login': new FormControl(),
      'pass':  new FormControl()
    })
  }

  loginSubmit(): void {
   this.loginService.checkPrivs(this.loginEditForm.value)
      .subscribe(
        (res) => {
          if(res === true) {
            localStorage.setItem('youCan','true');
            this.router.navigate(['/adminPage']);
          } else {
            localStorage.setItem('youCan', 'false');
          }
        },
        (err) => {console.log(err)}
      );
  }

}
