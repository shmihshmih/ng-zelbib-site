import { Component, OnInit } from '@angular/core';
import {MainService} from "../../service/main/main.service";
import {Pricelist} from "../../service/model/pricelist.model";

@Component({
  selector: 'app-price-list',
  templateUrl: './price-list.component.html',
  styleUrls: ['./price-list.component.scss']
})
export class PriceListComponent implements OnInit {
  services: Pricelist[] = [];
  constructor(
    private mainService: MainService
  ) {
    this.getServices();
  }

  ngOnInit() {
  }

  getServices() {
    this.mainService.getPricelists('v_pricelist')
        .subscribe(
          (services) => this.services = services || [],
          (err) => console.log(err)
        )
  }

}
