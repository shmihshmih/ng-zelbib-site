import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {MainService} from "../../service/main/main.service";
import {Article} from "../../service/model/article.model";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private mainService: MainService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      (val) => {this.getArticle(val.id)}
    );
  }

  article: Article;

  goBack(): void {
    this.location.back();
  }
  getArticle(id: any): void {
    this.mainService.getArticles('v_article')
      .subscribe(
        (articles) => {this.article = articles.find(article => article.id == id);}
      )
  }

}
