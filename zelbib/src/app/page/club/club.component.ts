import { Component, OnInit } from '@angular/core';
import {MainService} from "../../service/main/main.service";
import {Club} from "../../service/model/club.model";

@Component({
  selector: 'app-club',
  templateUrl: './club.component.html',
  styleUrls: ['./club.component.scss']
})
export class ClubComponent implements OnInit {
  clubs: Club[] = [];
  constructor(
    private mainService: MainService
  ) {
    this.getClubs();
  }

  ngOnInit() {
  }
   getClubs () {
    this.mainService.getClubs('v_club')
      .subscribe(
        (clubs) => {this.clubs = clubs || []}
      )
   }
}
