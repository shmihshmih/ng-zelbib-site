import { Component, OnInit } from '@angular/core';
import {MainService} from "../../service/main/main.service";
import {Recommendation} from "../../service/model/recommendation.model";

@Component({
  selector: 'app-recommendation',
  templateUrl: './recommendation.component.html',
  styleUrls: ['./recommendation.component.scss']
})
export class RecommendationComponent implements OnInit {
  recommendations: Recommendation[] = [];
  constructor(
    private mainService: MainService
  ) {
    this.getRecommendations();
  }

  ngOnInit() {
  }

  getRecommendations() {
    this.mainService.getRecommendations('v_recommendation')
      .subscribe(
        (recommendations) => {this.recommendations = recommendations || []}
      )
  }
}
