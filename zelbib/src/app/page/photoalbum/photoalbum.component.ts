import { Component, OnInit } from '@angular/core';
import {Album} from "../../service/model/album.model";
import {MainService} from "../../service/main/main.service";

@Component({
  selector: 'app-photoalbum',
  templateUrl: './photoalbum.component.html',
  styleUrls: ['./photoalbum.component.scss']
})
export class PhotoalbumComponent implements OnInit {

  albums: Album[] = [];

  constructor(
    private mainService: MainService
  ) { }

  ngOnInit() {
    this.getAlbums();
  }

  getAlbums(): void {
    this.mainService.getAlbums('v_album')
      .subscribe(
        (albums) => {this.albums = albums},
        (err) => {console.log(err)}
      )
  }

}
