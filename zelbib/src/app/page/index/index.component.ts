import { Component, OnInit } from '@angular/core';
import {MainService} from "../../service/main/main.service";
import {Affiche} from "../../service/model/affiche.model";
import {Today} from "../../service/model/today.model";
import {Flashbook} from "../../service/model/flashbook.model";
import {Article} from "../../service/model/article.model";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  flashbooks: Flashbook[] = [];
  todays: Today[] = [];
  affiches: Affiche[] = [];
  articles: Article[] = [];
  constructor(
    private mainService: MainService
  ) {
    this.getAffiches();
    this.getTodays();
    this.getFlashbooks();
    this.getArticles();
  }values

  ngOnInit() {
  }

  getAffiches() {
    this.mainService.getAffiches('v_affiche')
      .subscribe((affiches) => {this.affiches = affiches || []},
        (err) => {console.log(err)})
  }
  getTodays() {
    this.mainService.getTodays('v_today')
      .subscribe((todays) => {this.todays = todays || []},
        (err) => {console.log(err)})
  }
  getFlashbooks() {
    this.mainService.getFlashbooks('v_flashbook')
      .subscribe((flashbooks) => {this.flashbooks = flashbooks || []},
        (err) => {console.log(err)})
  }
  getArticles() {
    this.mainService.getArticles('v_article')
      .subscribe((articles) => {this.articles = articles.splice(0,7) || []},
        (err) => {console.log(err)})
  }
}
