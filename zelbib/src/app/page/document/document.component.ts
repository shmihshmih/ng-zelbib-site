import { Component, OnInit } from '@angular/core';
import {MainService} from "../../service/main/main.service";
import {myDocument} from "../../service/model/document.model";

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {
  documents: myDocument[] = [];

  constructor(
    private mainService: MainService
  ) {
    this.getDocuments();
  }

  ngOnInit() {
  }

  getDocuments() {
    this.mainService.getDocuments('v_document')
      .subscribe(
        (documents) => {this.documents = documents || []}
      )
  }
}
