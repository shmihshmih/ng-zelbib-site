import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RecaptchaModule } from 'ng-recaptcha';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './page/index/index.component';
import { HeaderComponent } from './page-part/header/header.component';
import { AboutComponent } from './page/about/about.component';
import { AboutCityComponent } from './page/about-city/about-city.component';
import { AdminComponent } from './page/admin/admin.component';
import { ClubComponent } from './page/club/club.component';
import { CompetitionComponent } from './page/competition/competition.component';
import { DocumentComponent } from './page/document/document.component';
import { GuestBookComponent } from './page/guest-book/guest-book.component';
import { NewsComponent } from './page/news/news.component';
import { PhotoalbumComponent } from './page/photoalbum/photoalbum.component';
import { PriceListComponent } from './page/price-list/price-list.component';
import { RecommendationComponent } from './page/recommendation/recommendation.component';
import { NavigatorComponent } from './page-part/navigator/navigator.component';
import { FooterComponent } from './page-part/footer/footer.component';
import {AdminService} from "./service/admin/admin.service";
import {MainService} from "./service/main/main.service";
import {AdminGuard} from "./guard/admin/admin.guard";
import { MagazineFilterPipe } from './pipe/magazine-filter.pipe';
import { ArticleComponent } from './page/article/article.component';
import { LoginComponent } from './page/login/login.component';
import {LoginService} from "./service/login/login.service";
import {Angular2ImageGalleryModule} from "angular2-image-gallery";
import { PhotoComponent } from './page/photo/photo.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    HeaderComponent,
    AboutComponent,
    AboutCityComponent,
    AdminComponent,
    ClubComponent,
    CompetitionComponent,
    DocumentComponent,
    GuestBookComponent,
    NewsComponent,
    PhotoalbumComponent,
    PriceListComponent,
    RecommendationComponent,
    NavigatorComponent,
    FooterComponent,
    MagazineFilterPipe,
    ArticleComponent,
    LoginComponent,
    PhotoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule,
    Angular2ImageGalleryModule,
    RecaptchaModule.forRoot()
  ],
  providers: [AdminService, MainService, AdminGuard, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
