import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {IndexComponent} from "./page/index/index.component";
import {AboutComponent} from "./page/about/about.component";
import {AboutCityComponent} from "./page/about-city/about-city.component";
import {AdminComponent} from "./page/admin/admin.component";
import {ClubComponent} from "./page/club/club.component";
import {CompetitionComponent} from "./page/competition/competition.component";
import {DocumentComponent} from "./page/document/document.component";
import {GuestBookComponent} from "./page/guest-book/guest-book.component";
import {NewsComponent} from "./page/news/news.component";
import {PhotoalbumComponent} from "./page/photoalbum/photoalbum.component";
import {PriceListComponent} from "./page/price-list/price-list.component";
import {RecommendationComponent} from "./page/recommendation/recommendation.component";
import {ArticleComponent} from "./page/article/article.component";
import {AdminGuard} from "./guard/admin/admin.guard";
import {LoginComponent} from "app/page/login/login.component";
import {PhotoComponent} from "./page/photo/photo.component";

const routes: Routes = [
  {
    path: 'indexPage',
    component: IndexComponent,
    children: []
  },
  {
    path: 'aboutPage',
    component: AboutComponent,
    children: []
  },
  {
    path: 'aboutCityPage',
    component: AboutCityComponent,
    children: []
  },
  {
    path: 'adminPage',
    component: AdminComponent,
    children: [],
    canActivate: [AdminGuard]
  },
  {
    path: 'clubPage',
    component: ClubComponent,
    children: []
  },
  {
    path: 'competitionPage',
    component: CompetitionComponent,
    children: []
  },
  {
    path: 'documentPage',
    component: DocumentComponent,
    children: []
  },
  {
    path: 'guestBookPage',
    component: GuestBookComponent,
    children: []
  },
  {
    path: 'newsPage',
    component: NewsComponent,
    children: []
  },
  {
    path: 'photoalbumPage',
    component: PhotoalbumComponent,
    children: []
  },
  {
    path: 'priceListPage',
    component: PriceListComponent,
    children: []
  },
  {
    path: 'recommendationPage',
    component: RecommendationComponent,
    children: []
  },
  {
    path: '',
    component: IndexComponent,
    children: []
  },
  {
    path: 'articlePage/:id',
    component: ArticleComponent,
    children: []
  },
  {
    path: 'login',
    component: LoginComponent,
    children: []
  },
  {
    path: 'photoalbumPage/:id',
    component: PhotoComponent,
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
