import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'magazineFilter'
})
export class MagazineFilterPipe implements PipeTransform {

  transform(items: any, args?: any): any {
    if (!items || !args) {
      return items;
    }
    return items.filter(item => item.periodic_type_caption == args)
  }

}
