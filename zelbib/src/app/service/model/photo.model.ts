export class Photo {
  id: number;
  caption: string;
  link: string;
  pid: number;
}