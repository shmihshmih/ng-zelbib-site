export class Album {
  id: number;
  date_of: string;
  title: string;
  cover: string;
}