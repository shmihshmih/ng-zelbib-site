export class Competition {
  id: number;
  caption: string;
  description: string;
  dates: string;
  age: string;
}