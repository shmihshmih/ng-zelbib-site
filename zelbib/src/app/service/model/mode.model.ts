export class Mode {
  id: number;
  holiday: string;
  working: string;
  lunch: string;
  last_day: string;
  season: string;
}