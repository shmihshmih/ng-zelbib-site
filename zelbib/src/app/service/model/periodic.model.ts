export class Periodic {
  id: number;
  caption: string;
  period: string;
  periodic_type: number;
  language: string;
  periodic_type_caption: string;
}