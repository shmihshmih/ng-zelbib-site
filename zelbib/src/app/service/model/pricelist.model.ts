export class Pricelist {
  id: number;
  caption: string;
  pid: number;
  cost: string;
  per_one: string;
}