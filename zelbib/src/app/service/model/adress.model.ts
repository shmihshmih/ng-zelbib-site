export class Adress {
  id: number;
  adress: string;
  phone: string;
  email: string;
  site: string;
  telegram: string;
  watsapp: string;
  vk: string;
}