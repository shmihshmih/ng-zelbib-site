export class Club {
  id: number;
  caption: string;
  description: string;
  dates: string;
  age: string;
}