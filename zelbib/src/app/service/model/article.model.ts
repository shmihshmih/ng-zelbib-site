export class Article {
  id: number;
  date_of: string;
  title: string;
  preview_annotation: string;
  content: string;
  preview_image: string;
  content_image: string;
  author: string;
}