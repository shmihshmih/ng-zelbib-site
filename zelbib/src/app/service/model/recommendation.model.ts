export class Recommendation {
  id: number;
  rubric: string;
  caption: string;
  description: string;
  link: string;
}