export class Flashbook {
  id: number;
  caption: string;
  link: string;
  img: string;
}