import { Injectable } from '@angular/core';
import { Http, RequestOptions, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/throw';

import { _set } from '../../setting.conf';
import {Today} from "../model/today.model";
import {Article} from "../model/article.model";
import {Periodic} from "../model/periodic.model";
import {Club} from "../model/club.model";
import {Pricelist} from "../model/pricelist.model";
import {Competition} from "../model/competition.model";
import {Recommendation} from "../model/recommendation.model";
import {Affiche} from "../model/affiche.model";
import {AboutCity} from "../model/about_city.model";
import {Album} from "../model/album.model";
import {Flashbook} from "../model/flashbook.model";
import {Material} from "../model/material.model";
import {myDocument} from "../model/document.model";
import {Mode} from "../model/mode.model";
import {Adress} from "../model/adress.model";
import {Photo} from "../model/photo.model";

@Injectable()
export class AdminService {

  headers = new Headers({'content-type':'application/json;charset=utf-8'});
  options = new RequestOptions({headers: this.headers});
  constructor(
    private http: Http
  ) { }


  //views
  getUsers(viewName: string): Observable<any[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }

  //dels
  delToday(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delArticle(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delPeriodic(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delClub(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delPricelist(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delCompetition(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delRecommendation(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delAffiche(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delAbout_city(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delAlbum(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delFlashbook(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delDocument(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delMaterial(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delPhoto(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  delGuest_book(del: number, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { del }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  //mods
  modToday(today: Today, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { today }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modArticle(article: Article, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { article }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modPeriodic(periodic: Periodic, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { periodic }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modClub(club: Club, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { club }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modPricelist(pricelist: Pricelist, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { pricelist }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modCompetition(competition: Competition, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { competition }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modRecommendation(recommendation: Recommendation, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { recommendation }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modAffiche(affiche: Affiche, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { affiche }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modAbout_city(about_city: AboutCity, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { about_city }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modAlbum(album: Album, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { album }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modFlashbook(flashbook: Flashbook, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { flashbook }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modDocument(mydocument: myDocument, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { mydocument }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modMaterial(material: Material, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { material }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modMode(mode: Mode, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { mode }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modAdress(adress: Adress, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { adress }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modPhoto(photo: Photo, table: string): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { photo }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modGuest_book(guest_book: any, table: string): Observable<any> {
    console.log('hello from service');
    console.log(guest_book);
    return this.http
      .post(_set.modelPath+'m_'+table+'.php', { guest_book }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  //common
  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }
  private  handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

}
