import {Injectable, OnInit} from '@angular/core';
import { Http, RequestOptions, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/throw';

import { _set } from '../../setting.conf';

import { AboutCity } from '../model/about_city.model';
import {Adress} from "../model/adress.model";
import {Affiche} from "../model/affiche.model";
import {Album} from "../model/album.model";
import {Article} from "../model/article.model";
import {Club} from "../model/club.model";
import {Competition} from "../model/competition.model";
import {Flashbook} from "../model/flashbook.model";
import {GuestBook} from "../model/guest_book.model";
import {Material} from "../model/material.model";
import {Mode} from "../model/mode.model";
import {Periodic} from "../model/periodic.model";
import {PeriodicType} from "../model/periodic_type.model";
import {Pricelist} from "../model/pricelist.model";
import {Recommendation} from "../model/recommendation.model";
import {Today} from "../model/today.model";
import {myDocument} from "../model/document.model";
import {Photo} from "../model/photo.model";

@Injectable()
export class MainService implements OnInit{

  headers = new Headers({'content-type':'application/json;charset=utf-8'});
  options = new RequestOptions({headers: this.headers});

  constructor(
    private http: Http
  ) { }
  ngOnInit(): void {}

  //view
  getAboutCities(viewName: string): Observable<AboutCity[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getAdresses(viewName: string): Observable<Adress> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getAffiches(viewName: string): Observable<Affiche[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getAlbums(viewName: string): Observable<Album[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getArticles(viewName: string): Observable<Article[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getClubs(viewName: string): Observable<Club[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getCompetitions(viewName: string): Observable<Competition[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getDocuments(viewName: string): Observable<myDocument[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getFlashbooks(viewName: string): Observable<Flashbook[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getGuest_book(viewName: string): Observable<GuestBook[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getLogs(viewName: string): Observable<any[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getMaterials(viewName: string): Observable<Material[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getModes(viewName: string): Observable<Mode[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getPeriodics(viewName: string): Observable<Periodic[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getPeriodicTypes(viewName: string): Observable<PeriodicType[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getPricelists(viewName: string): Observable<Pricelist[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getRecommendations(viewName: string): Observable<Recommendation[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getTodays(viewName: string): Observable<Today[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getTodaysList(viewName: string): Observable<Today[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getPhotos_for_admin(viewName: string): Observable<Photo[]> {
    return this.http
      .get(_set.viewPath+viewName+'.php')
      .map(this.extractData)
      .catch(this.handleError)
  }
  getPhotos(viewName: string, albumId: number): Observable<any[]> {
    let album = {
      'id': albumId
    };
    return this.http
      .get(_set.viewPath+viewName+'.php?pid='+album.id)
      .map(this.extractData)
      .catch(this.handleError);
  }
  //common
  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }
  private  handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }
}
