import { Injectable } from '@angular/core';
import { Http, RequestOptions, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/throw';

import { _set } from '../../setting.conf';

@Injectable()
export class LoginService {
  headers = new Headers({'content-type':'application/json;charset=utf-8'});
  options = new RequestOptions({headers: this.headers});

  constructor(
    private http: Http
  ) { }

  checkPrivs(login): Observable<any> {
    return this.http
      .post(_set.modelPath+'m_login.php', { login }, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  //common
  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }
  private  handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

}
