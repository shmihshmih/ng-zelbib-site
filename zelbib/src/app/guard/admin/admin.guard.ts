import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AdminGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean
  {
    //return confirm('Вы уверены, что хотите перейти?');
    //TODO: по прямой ссылке нельзы, проверки из логина потом запилист
    if (localStorage.getItem('youCan') === 'true') {
      return true
    } else {
      return false;
    }
  }
}
